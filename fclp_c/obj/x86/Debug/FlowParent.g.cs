﻿#pragma checksum "..\..\..\FlowParent.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "EABF8E8864CA61DAE4BE9DE838457D23"
//------------------------------------------------------------------------------
// <auto-generated>
//     このコードはツールによって生成されました。
//     ランタイム バージョン:4.0.30319.18052
//
//     このファイルへの変更は、以下の状況下で不正な動作の原因になったり、
//     コードが再生成されるときに損失したりします。
// </auto-generated>
//------------------------------------------------------------------------------

using Microsoft.Expression.Controls;
using Microsoft.Expression.Interactivity.Core;
using Microsoft.Expression.Interactivity.Input;
using Microsoft.Expression.Interactivity.Layout;
using Microsoft.Expression.Interactivity.Media;
using Microsoft.Expression.Media;
using Microsoft.Expression.Media.Effects;
using Microsoft.Expression.Shapes;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using fclp_c;


namespace fclp_c {
    
    
    /// <summary>
    /// FlowParent
    /// </summary>
    public partial class FlowParent : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 304 "..\..\..\FlowParent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Media.Animation.BeginStoryboard Initialize_BeginStoryboard;
        
        #line default
        #line hidden
        
        
        #line 307 "..\..\..\FlowParent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Media.Animation.BeginStoryboard Title_Show_BeginStoryboard;
        
        #line default
        #line hidden
        
        
        #line 310 "..\..\..\FlowParent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Media.Animation.BeginStoryboard Title_Hide_BeginStoryboard;
        
        #line default
        #line hidden
        
        
        #line 313 "..\..\..\FlowParent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Media.Animation.BeginStoryboard Title_Show_BeginStoryboard1;
        
        #line default
        #line hidden
        
        
        #line 316 "..\..\..\FlowParent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Media.Animation.BeginStoryboard SerialPort_Show_BeginStoryboard;
        
        #line default
        #line hidden
        
        
        #line 319 "..\..\..\FlowParent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Media.Animation.BeginStoryboard SerialPort_Hide_BeginStoryboard;
        
        #line default
        #line hidden
        
        
        #line 322 "..\..\..\FlowParent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Media.Animation.BeginStoryboard Drawing_Show_BeginStoryboard;
        
        #line default
        #line hidden
        
        
        #line 325 "..\..\..\FlowParent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Media.Animation.BeginStoryboard Drawing_Hide_BeginStoryboard1;
        
        #line default
        #line hidden
        
        
        #line 328 "..\..\..\FlowParent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Media.Animation.BeginStoryboard Drawing_Hide_BeginStoryboard;
        
        #line default
        #line hidden
        
        
        #line 331 "..\..\..\FlowParent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Media.Animation.BeginStoryboard Option_Hide_BeginStoryboard;
        
        #line default
        #line hidden
        
        
        #line 335 "..\..\..\FlowParent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.WrapPanel XNotifyPanel;
        
        #line default
        #line hidden
        
        
        #line 336 "..\..\..\FlowParent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid XSerialPortFrow;
        
        #line default
        #line hidden
        
        
        #line 351 "..\..\..\FlowParent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox XPortNameTextBox;
        
        #line default
        #line hidden
        
        
        #line 352 "..\..\..\FlowParent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button XOpenButton;
        
        #line default
        #line hidden
        
        
        #line 353 "..\..\..\FlowParent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button XCloseButton;
        
        #line default
        #line hidden
        
        
        #line 354 "..\..\..\FlowParent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox XIsEnabledCheckBox;
        
        #line default
        #line hidden
        
        
        #line 355 "..\..\..\FlowParent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button XShowSendImageButton;
        
        #line default
        #line hidden
        
        
        #line 356 "..\..\..\FlowParent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button XCloseSendImageButton;
        
        #line default
        #line hidden
        
        
        #line 357 "..\..\..\FlowParent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox XExportCSVDataCheckBox;
        
        #line default
        #line hidden
        
        
        #line 358 "..\..\..\FlowParent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox XUseAsyncSendCheckBox;
        
        #line default
        #line hidden
        
        
        #line 361 "..\..\..\FlowParent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border XSerialPortTrigger;
        
        #line default
        #line hidden
        
        
        #line 374 "..\..\..\FlowParent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid XDrawingFrow;
        
        #line default
        #line hidden
        
        
        #line 387 "..\..\..\FlowParent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border XDrawingTrigger;
        
        #line default
        #line hidden
        
        
        #line 401 "..\..\..\FlowParent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBox XDrawingList;
        
        #line default
        #line hidden
        
        
        #line 402 "..\..\..\FlowParent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton XAnimationRadioButton;
        
        #line default
        #line hidden
        
        
        #line 403 "..\..\..\FlowParent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton XShowTextRadioButton;
        
        #line default
        #line hidden
        
        
        #line 404 "..\..\..\FlowParent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton XDirectShowRadioButton;
        
        #line default
        #line hidden
        
        
        #line 405 "..\..\..\FlowParent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton XMicInputRadioButton;
        
        #line default
        #line hidden
        
        
        #line 406 "..\..\..\FlowParent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton XShowImageRadioButton;
        
        #line default
        #line hidden
        
        
        #line 407 "..\..\..\FlowParent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton XShowTestRadioButton;
        
        #line default
        #line hidden
        
        
        #line 408 "..\..\..\FlowParent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton XTetrisRadioButton;
        
        #line default
        #line hidden
        
        
        #line 410 "..\..\..\FlowParent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button XOpenWindowButton;
        
        #line default
        #line hidden
        
        
        #line 414 "..\..\..\FlowParent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid XOptionFrow;
        
        #line default
        #line hidden
        
        
        #line 427 "..\..\..\FlowParent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border XOptionTrigger;
        
        #line default
        #line hidden
        
        
        #line 441 "..\..\..\FlowParent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button XProgramOptionButton;
        
        #line default
        #line hidden
        
        
        #line 442 "..\..\..\FlowParent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button XNotifySettingButton;
        
        #line default
        #line hidden
        
        
        #line 446 "..\..\..\FlowParent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid XTitleFrow;
        
        #line default
        #line hidden
        
        
        #line 457 "..\..\..\FlowParent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid XEndButton;
        
        #line default
        #line hidden
        
        
        #line 481 "..\..\..\FlowParent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Shapes.Path XTitleTrigger;
        
        #line default
        #line hidden
        
        
        #line 482 "..\..\..\FlowParent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label;
        
        #line default
        #line hidden
        
        
        #line 483 "..\..\..\FlowParent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ProgressBar XSendIndicator;
        
        #line default
        #line hidden
        
        
        #line 485 "..\..\..\FlowParent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image XSplashLogoImage;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/fclp_c;component/flowparent.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\FlowParent.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.Initialize_BeginStoryboard = ((System.Windows.Media.Animation.BeginStoryboard)(target));
            return;
            case 2:
            this.Title_Show_BeginStoryboard = ((System.Windows.Media.Animation.BeginStoryboard)(target));
            return;
            case 3:
            this.Title_Hide_BeginStoryboard = ((System.Windows.Media.Animation.BeginStoryboard)(target));
            return;
            case 4:
            this.Title_Show_BeginStoryboard1 = ((System.Windows.Media.Animation.BeginStoryboard)(target));
            return;
            case 5:
            this.SerialPort_Show_BeginStoryboard = ((System.Windows.Media.Animation.BeginStoryboard)(target));
            return;
            case 6:
            this.SerialPort_Hide_BeginStoryboard = ((System.Windows.Media.Animation.BeginStoryboard)(target));
            return;
            case 7:
            this.Drawing_Show_BeginStoryboard = ((System.Windows.Media.Animation.BeginStoryboard)(target));
            return;
            case 8:
            this.Drawing_Hide_BeginStoryboard1 = ((System.Windows.Media.Animation.BeginStoryboard)(target));
            return;
            case 9:
            this.Drawing_Hide_BeginStoryboard = ((System.Windows.Media.Animation.BeginStoryboard)(target));
            return;
            case 10:
            this.Option_Hide_BeginStoryboard = ((System.Windows.Media.Animation.BeginStoryboard)(target));
            return;
            case 11:
            this.XNotifyPanel = ((System.Windows.Controls.WrapPanel)(target));
            return;
            case 12:
            this.XSerialPortFrow = ((System.Windows.Controls.Grid)(target));
            return;
            case 13:
            this.XPortNameTextBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 14:
            this.XOpenButton = ((System.Windows.Controls.Button)(target));
            
            #line 352 "..\..\..\FlowParent.xaml"
            this.XOpenButton.Click += new System.Windows.RoutedEventHandler(this.XOpenButton_Click);
            
            #line default
            #line hidden
            return;
            case 15:
            this.XCloseButton = ((System.Windows.Controls.Button)(target));
            
            #line 353 "..\..\..\FlowParent.xaml"
            this.XCloseButton.Click += new System.Windows.RoutedEventHandler(this.XCloseButton_Click);
            
            #line default
            #line hidden
            return;
            case 16:
            this.XIsEnabledCheckBox = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 17:
            this.XShowSendImageButton = ((System.Windows.Controls.Button)(target));
            
            #line 355 "..\..\..\FlowParent.xaml"
            this.XShowSendImageButton.Click += new System.Windows.RoutedEventHandler(this.XShowSendImageButton_Click);
            
            #line default
            #line hidden
            return;
            case 18:
            this.XCloseSendImageButton = ((System.Windows.Controls.Button)(target));
            
            #line 356 "..\..\..\FlowParent.xaml"
            this.XCloseSendImageButton.Click += new System.Windows.RoutedEventHandler(this.XCloseSendImageButton_Click);
            
            #line default
            #line hidden
            return;
            case 19:
            this.XExportCSVDataCheckBox = ((System.Windows.Controls.CheckBox)(target));
            
            #line 357 "..\..\..\FlowParent.xaml"
            this.XExportCSVDataCheckBox.Checked += new System.Windows.RoutedEventHandler(this.XExportCSVDataCheckBox_Checked);
            
            #line default
            #line hidden
            return;
            case 20:
            this.XUseAsyncSendCheckBox = ((System.Windows.Controls.CheckBox)(target));
            
            #line 358 "..\..\..\FlowParent.xaml"
            this.XUseAsyncSendCheckBox.Checked += new System.Windows.RoutedEventHandler(this.XUseAsyncSendCheckBox_Checked);
            
            #line default
            #line hidden
            return;
            case 21:
            this.XSerialPortTrigger = ((System.Windows.Controls.Border)(target));
            return;
            case 22:
            this.XDrawingFrow = ((System.Windows.Controls.Grid)(target));
            return;
            case 23:
            this.XDrawingTrigger = ((System.Windows.Controls.Border)(target));
            return;
            case 24:
            this.XDrawingList = ((System.Windows.Controls.ListBox)(target));
            return;
            case 25:
            this.XAnimationRadioButton = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 26:
            this.XShowTextRadioButton = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 27:
            this.XDirectShowRadioButton = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 28:
            this.XMicInputRadioButton = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 29:
            this.XShowImageRadioButton = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 30:
            this.XShowTestRadioButton = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 31:
            this.XTetrisRadioButton = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 32:
            this.XOpenWindowButton = ((System.Windows.Controls.Button)(target));
            
            #line 410 "..\..\..\FlowParent.xaml"
            this.XOpenWindowButton.Click += new System.Windows.RoutedEventHandler(this.XOpenWindowButton_Click);
            
            #line default
            #line hidden
            return;
            case 33:
            this.XOptionFrow = ((System.Windows.Controls.Grid)(target));
            return;
            case 34:
            this.XOptionTrigger = ((System.Windows.Controls.Border)(target));
            return;
            case 35:
            this.XProgramOptionButton = ((System.Windows.Controls.Button)(target));
            
            #line 441 "..\..\..\FlowParent.xaml"
            this.XProgramOptionButton.Click += new System.Windows.RoutedEventHandler(this.XProgramOptionButton_Click);
            
            #line default
            #line hidden
            return;
            case 36:
            this.XNotifySettingButton = ((System.Windows.Controls.Button)(target));
            return;
            case 37:
            this.XTitleFrow = ((System.Windows.Controls.Grid)(target));
            return;
            case 38:
            this.XEndButton = ((System.Windows.Controls.Grid)(target));
            
            #line 457 "..\..\..\FlowParent.xaml"
            this.XEndButton.MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.XEndButton_MouseLeftButtonDown);
            
            #line default
            #line hidden
            return;
            case 39:
            this.XTitleTrigger = ((System.Windows.Shapes.Path)(target));
            return;
            case 40:
            this.label = ((System.Windows.Controls.Label)(target));
            return;
            case 41:
            this.XSendIndicator = ((System.Windows.Controls.ProgressBar)(target));
            return;
            case 42:
            this.XSplashLogoImage = ((System.Windows.Controls.Image)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

