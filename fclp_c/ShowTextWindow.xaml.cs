﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using OpenCvSharp;
using System.IO;

namespace fclp_c
{
	/// <summary>
	/// ShowTextWindow.xaml の相互作用ロジック
	/// </summary>
	public partial class ShowTextWindow : Window,IFPGASendable
	{
		//thread
		public FPGASender.SendFromIplImageDelegate SendFromIplImage { get; set; }
		public FPGASender.NotifyMessageDelegate NotifyMessage { get; set; }
		int _interval = FPGASender.MASTER_INTERVAL;//Tick[ms]
		DispatcherTimer _updateThread = null;
		//image
		IplImage _sendImageBase = null;
		CvWindow _previewBaseImageWindow = null;
		CvWindow _previewCropImageWindow = null;
		//image
		string _imageDirName = "image";
		string _imagePath = string.Empty;

		public ShowTextWindow()
		{
			InitializeComponent();
		}
		//////////////////////////////////////////////////////////////////////////////////////////////
		//Delegate
		bool IFPGASendable.Initialize()
		{
			_previewBaseImageWindow = new CvWindow("Base Image", WindowMode.FreeRatio);
			_previewCropImageWindow = new CvWindow("Crop Image", WindowMode.FreeRatio);
			InitializeThread();
			return true;
		}

		void IFPGASendable.Stop()
		{
			FinalizeThread();
		}

		bool IFPGASendable.Finalize()
		{
			FinalizeThread();
			if (_sendImageBase != null)
			{
				_sendImageBase.Dispose();
				_sendImageBase = null;
			}
			if (_previewBaseImageWindow != null)
			{
				_previewBaseImageWindow.Dispose();
				_previewBaseImageWindow = null;
			}
			if (_previewCropImageWindow != null)
			{
				_previewCropImageWindow.Dispose();
				_previewCropImageWindow = null;
			}
			return true;
		}
		//////////////////////////////////////////////////////////////////////////////////////////////
		//Thread
		private void InitializeThread()
		{
			FinalizeThread();

			_updateThread = new DispatcherTimer();
			_updateThread.Interval = new TimeSpan(0, 0, 0, 0, _interval);
			_updateThread.Tick += new EventHandler(_updateThread_Tick);
			_updateThread.Start();


		}
		private void ChangeThreadInterval()
		{
			if (_updateThread != null)
			{
				_updateThread.Stop();
				_updateThread.Interval = new TimeSpan(0, 0, 0, 0, _interval);
				_updateThread.Start();
			}

		}
		private void FinalizeThread()
		{
			if (_updateThread != null)
			{
				if (_updateThread.IsEnabled) _updateThread.Stop();
				_updateThread = null;
			}
		}
		//DispatcherTimer
		void _updateThread_Tick(object sender, EventArgs e)
		{
			if (_sendImageBase == null) return;
			//AutoSlide
			if (XEnableAutoScrollCheckBox.IsChecked == true)
			{
				++XPositionSlider.Value;
				//reset
				if (XPositionSlider.Value == XPositionSlider.Maximum)
				{
					XPositionSlider.Value = 0;
				}
			}

			//ROI
			Cv.SetImageROI(_sendImageBase, new CvRect((int)XPositionSlider.Value, (int)YPositionSlider.Value, FPGASender.ALL_WIDTH, FPGASender.ALL_HEIGHT));
			IplImage crop_image = Cv.CloneImage(_sendImageBase);
			Cv.ResetImageROI(_sendImageBase);

			//プレビュー
			IplImage copy_image = Cv.CloneImage(_sendImageBase);
			if (_previewBaseImageWindow != null) _previewBaseImageWindow.ShowImage(copy_image);
			if (_previewCropImageWindow != null) _previewCropImageWindow.ShowImage(crop_image);

			//送信
			SendFromIplImage(crop_image, false);

			//開放
			crop_image.Dispose();
			crop_image = null;
			copy_image.Dispose();
			copy_image = null;
		}
		//////////////////////////////////////////////////////////////////////////////////////////////
		//Event
		private void XCreateButton_Click(object sender, RoutedEventArgs e)
		{
			if (string.IsNullOrWhiteSpace(XCreateTextBox.Text))
			{
				NotifyMessage(this, "文字が入力されていません。");
				return;
			}
			
			if (!Directory.Exists(_imageDirName))
			{
				Directory.CreateDirectory(_imageDirName);
				NotifyMessage(this, "{0}を作成しました。文字列から生成した画像が格納されます。", _imageDirName);
			}
			//生成
			var image_bitmap = ImageHelper.DrawStringForLEDPanel(XCreateTextBox.Text,XRSlider.Value,XGSlider.Value,XBSlider.Value);
			//保存パスを生成
			Random r = new Random();
			_imagePath = string.Format("{0}\\{1}.bmp", _imageDirName, r.Next());
			//保存
			image_bitmap.Save(_imagePath);
			//開放
			image_bitmap.Dispose();
			image_bitmap = null;

			//読み込み
			if (_sendImageBase != null)
			{
				_sendImageBase.Dispose();
				_sendImageBase = null;
			}
			try
			{
				_sendImageBase = new IplImage(_imagePath, LoadMode.AnyColor);
			}
			catch (Exception ex)
			{
				NotifyMessage(ex.Source, "画像の読み込みに失敗しました。\n{0}\n{1}", ex.Message, ex.StackTrace);
				return;
			}
			//スライダーの設定
			XPositionSlider.Value = 0;
			YPositionSlider.Value = 11;//TODO:ちゃんと決めようぜ
			XPositionSlider.Maximum = _sendImageBase.Width - FPGASender.ALL_WIDTH;
			YPositionSlider.Maximum = _sendImageBase.Height - FPGASender.ALL_HEIGHT;
			XPositionSlider.LargeChange = FPGASender.PANEL_WIDTH;
			YPositionSlider.LargeChange = YPositionSlider.Maximum / 10;//FPGASender.PANEL_HEIGHT;
			XPositionSlider.TickFrequency = XPositionSlider.LargeChange;
			YPositionSlider.TickFrequency = YPositionSlider.LargeChange;
			XPositionSlider.SmallChange = 1;
			YPositionSlider.SmallChange = 1;

			//Scrollを表示
			XHiddenEnableScrollCheckBox.IsChecked = true;

			this.Title = string.Format("ShowTextWindow text = {0}",XCreateTextBox.Text);
			NotifyMessage(this, "画像を生成しました。\n{0}\n{1}", XCreateTextBox.Text, _imagePath);
		}

		private void XPositionSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
		{
			XPositionText.Content = string.Format("{0}:{1}", XPositionSlider.Value, XPositionSlider.Maximum);
		}

		private void YPositionSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
		{
			YPositionText.Content = string.Format("{0}:{1}", YPositionSlider.Value, YPositionSlider.Maximum);
		}

		private void XChangeAutoScrollSpeedButton_Click(object sender, RoutedEventArgs e)
		{
			//スレッドの実行頻度の変更
			_interval = (int)XAutoScrollSpeedSlider.Value;
			ChangeThreadInterval();

			NotifyMessage(this, "更新頻度を変更しました。\nInterval = {0}[m/s]", _interval);
		}

		private void XEnableAutoScrollCheckBox_Checked(object sender, RoutedEventArgs e)
		{
			NotifyMessage(this, "自動スクロールを有効にしました。");
		}

	}
}
