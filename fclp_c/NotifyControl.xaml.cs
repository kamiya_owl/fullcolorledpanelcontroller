﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace fclp_c
{
	/// <summary>
	/// NotifyControl.xaml の相互作用ロジック
	/// </summary>
	public partial class NotifyControl : UserControl
	{
		public bool IsEnabledAnimationBind
		{
			get { return (bool)this.XHiddenCheckBox.IsChecked; }
			set { this.XHiddenCheckBox.IsChecked = value; }
		}
		public NotifyControl(string calledby, string description)
		{
			InitializeComponent();
			SetData(calledby, description);
		}
		public void SetData(string calledby,string description)
		{
			this.XCalledByText.Text = calledby;
			this.XDescriptionText.Text = description;
			IsEnabledAnimationBind = true;
		}

		private void XDescriptionText_MouseDown(object sender, MouseButtonEventArgs e)
		{
			MessageBox.Show(this.XDescriptionText.Text, this.XCalledByText.Text, MessageBoxButton.OK, MessageBoxImage.None);
		}
	}
}
