﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Threading;
using OpenCvSharp;
using System.Diagnostics;

namespace fclp_c
{
    /// <summary>
    /// AnimationWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class AnimationWindow : Window, IFPGASendable
    {
        //delegate
        public FPGASender.SendFromIplImageDelegate SendFromIplImage { get; set; }
        public FPGASender.NotifyMessageDelegate NotifyMessage { get; set; }
        //描画計算用
        private volatile int fps = 24;
        private volatile int frameCount = 0;

        private int To8bitMap(double a) { return (int)(a * 0xff); }
        private double frameRatingSaw { get { return (double)(frameCount % fps) / (double)fps; } }
        private double frameRatingSawInv { get { return 1 - ((double)(frameCount % fps) / (double)fps); } }
        private double frameRatingTri
        {
            get
            {
                return (((frameCount / fps) % 2) == 1) ? frameRatingSawInv : frameRatingSaw;
            }
        }
        private double frameRatingTriInv
        {
            get
            {
                return (((frameCount / fps) % 2) == 1) ? frameRatingSaw : frameRatingSawInv;
            }
        }

        private double frameRatingSin(double div = 1.0) {  return Math.Sin((2 * Math.PI / div ) * ((double)(frameCount) / (double)fps)); }
        private double frameRatingCos(double div = 1.0) {  return Math.Cos((2 * Math.PI / div ) * ((double)(frameCount) / (double)fps)); }
        private double frameRatingSinAbs(double div = 1.0) { return Math.Abs(frameRatingSin(div)); }
        private double frameRatingCosAbs(double div = 1.0) { return Math.Abs(frameRatingCos(div)); }

        private double frameRatingSawSq
        {
            get
            {
                double rate = frameRatingSaw;
                return rate * rate;
            }
        }
        private double frameRatingSawInvSq
        {
            get
            {
                double rate = frameRatingSawInv;
                return rate * rate;
            }
        }
        private double frameRatingTriSq
        {
            get
            {
                double rate = frameRatingTri;
                return rate * rate;
            }
        }
        private double frameRatingTriInvSq
        {
            get
            {
                double rate = frameRatingTriInv;
                return rate * rate;
            }
        }

        private double frameRatingSawSqrt
        {
            get
            {
                return Math.Sqrt(frameRatingSaw) ;
            }
        }
        private double frameRatingSawInvSqrt
        {
            get
            {
                return Math.Sqrt(frameRatingSawInv);
            }
        }
        private double frameRatingTriSqrt
        {
            get
            {
                return Math.Sqrt(frameRatingTri);
            }
        }
        private double frameRatingTriInvSqrt
        {
            get
            {
                return Math.Sqrt(frameRatingTriInv);
            }
        }

        private volatile Random random = new Random();
        private double frameRatingRnd { get { return random.NextDouble(); } }

        private Thread drawingThread = null;
        private CvWindow previewWindow = new CvWindow("AnimationPreview", WindowMode.FreeRatio);
        private Dictionary<int, Action<IplImage>> DrawFunctions = null;
        private volatile int selectedFunc = 0;
        volatile IplImage oldDrawing = null;


        public AnimationWindow()
        {
            InitializeComponent();
            //RadioButtonの順番通りの関数を定義していく
            DrawFunctions = new Dictionary<int,Action<IplImage>>()
            {
                {0,NoneDraw},
                {1,SimpleClockDraw},
                {2,FadeClockDraw},
                {3,BubblesShown},
                {4,RandomLine},
                {5,CountDownwithBar},
                {6,RectTurn},
                {7,SohgikenLogo},
            };

            for (int i = 0; i < XAnimationList.Children.Count; ++i )
            {
                var radio = XAnimationList.Children[i] as RadioButton;
                if (radio == null) continue;
                int index = i;//非同期加算対策

                radio.Checked += (s, e) =>
                    {
                        Cv.Set(oldDrawing, new CvColor(0, 0, 0));
                        selectedFunc = index;
                    };
            }
        }

        public bool Initialize()
        {
            drawingThread = new Thread(new ThreadStart(OnDraw));
            drawingThread.Start();

            return true;
        }
        public void Stop()
        {
            Finalize();
        }

        public bool Finalize()
        {
            if (drawingThread != null && drawingThread.IsAlive)
            {
                drawingThread.Abort();
                drawingThread = null;
            }
            if (previewWindow != null)
            {
                previewWindow.Dispose();
                previewWindow = null;
            }

            return true;
        }
        private void OnDraw()
        {
            
            //init
            using (oldDrawing = new IplImage(FPGASender.ALL_WIDTH, FPGASender.ALL_HEIGHT, BitDepth.U8, 3))
            {
                Cv.Set(oldDrawing, new CvColor(0, 0, 0));

                var sw = new Stopwatch();
                //loop
                while (true)
                {
                    using (var drawing = new IplImage(FPGASender.ALL_WIDTH, FPGASender.ALL_HEIGHT, BitDepth.U8, 3))
                    {
                        //timer
                        sw.Restart();
                        //frame count
                        ++frameCount;
                        //draw
                        DrawFunctions[selectedFunc](drawing);
                        //send
                        Dispatcher.BeginInvoke(new Action(() => 
                            {
                                using (var send = new IplImage(FPGASender.ALL_WIDTH, FPGASender.ALL_HEIGHT, BitDepth.U8, 3))
                                {
                                    SendFromIplImage(drawing, false);
                                }
                            }));
                        //preview
                        previewWindow.ShowImage(drawing);
                        //frame store
                        Cv.Copy(drawing, oldDrawing);//how to use -> CvCopy(oldDrawing,drawing)
                        //wait
                        Thread.Sleep(Math.Abs(1000 / fps - (int)sw.ElapsedMilliseconds));
                    }
                }
            }
        }
        //以下追加関数(スレッドセーフで作ること)
        private void NoneDraw(IplImage drawing)
        {
            Cv.Set(drawing, new CvColor(0xff, 0xff, 0xff));
        }
        private void SimpleClockDraw(IplImage drawing)
        {
            Cv.Set(drawing, new CvColor(0x00, 0x00, 0x00));
            var text = DateTime.Now.ToShortTimeString();
            var point = new CvPoint(0, (int)(FPGASender.ALL_HEIGHT * 0.8));
            var font = text.Length > 4 ? new CvFont(FontFace.HersheyPlain, 0.1, 2.75, 0, 2, LineType.AntiAlias) : new CvFont(FontFace.HersheyPlain, 0.7, 3, 0, 2, LineType.AntiAlias);
            var color = new CvColor(0xff, 0xff, 0xff);
            Cv.PutText(drawing, text, point, font, color);
        }
        private void FadeClockDraw(IplImage drawing)
        {
            Cv.Set(drawing, new CvScalar(To8bitMap(frameRatingSinAbs(10)), To8bitMap(frameRatingSinAbs(15)), 0x80));
            Cv.CvtColor(drawing, drawing, ColorConversion.HsvToRgb);

            var text = DateTime.Now.ToShortTimeString();
            var point = new CvPoint(0, (int)(FPGASender.ALL_HEIGHT * 0.8));
            var font = text.Length > 4 ? new CvFont(FontFace.HersheyPlain, 0.1, 2.75, 0, 2, LineType.Link8) : new CvFont(FontFace.HersheyPlain, 0.7, 3, 0, 2, LineType.AntiAlias);
            var color = new CvColor(0xff, 0xff, 0xff);

            Cv.PutText(drawing, text, point, font, color);
        }
        private void BubblesShown(IplImage drawing)
        {
            //loop
            using (var blank = new IplImage(drawing.Size, BitDepth.U8, 3))
            {
                //old frame
                Cv.Copy(oldDrawing, drawing);

                double alpha = 0.9;
                Cv.AddWeighted(blank, 1 - alpha, drawing, alpha, 0.0, drawing);

                if (frameRatingRnd > 0.9)
                {
                    var color = new CvColor(To8bitMap(frameRatingRnd), To8bitMap(frameRatingRnd), To8bitMap(frameRatingRnd));
                    int x = (int)(frameRatingRnd * FPGASender.ALL_WIDTH);
                    int y = (int)(frameRatingRnd * FPGASender.ALL_HEIGHT);
                    int r = (int)(frameRatingRnd * FPGASender.PANEL_WIDTH + FPGASender.PANEL_WIDTH);

                    Cv.DrawCircle(drawing, x, y, r, color, -1);
                }
            }
        }
        private void RandomLine(IplImage drawing)
        {
            //loop
            using (var blank = new IplImage(drawing.Size, BitDepth.U8, 3))
            {
                //old frame
                Cv.Copy(oldDrawing, drawing);

                double alpha = 0.9;
                Cv.AddWeighted(blank, 1 - alpha, drawing, alpha, 0.0, drawing);

                if (frameRatingRnd > 0.4)
                {
                    var color = new CvColor(To8bitMap(frameRatingRnd), To8bitMap(frameRatingRnd), To8bitMap(frameRatingRnd));
                    int x1 = (int)(frameRatingRnd * FPGASender.ALL_WIDTH * 2 - FPGASender.ALL_WIDTH);
                    int y1 = (int)(frameRatingRnd * FPGASender.ALL_HEIGHT * 2 - FPGASender.ALL_HEIGHT);
                    int x2 = (int)(frameRatingRnd * FPGASender.ALL_WIDTH * 2 - FPGASender.ALL_WIDTH);
                    int y2 = (int)(frameRatingRnd * FPGASender.ALL_HEIGHT * 2 - FPGASender.ALL_HEIGHT);

                    Cv.DrawLine(drawing, x1, y1, x2, y2, color, 2, LineType.AntiAlias);
                }
            }
        }
        private bool CountDownwithBar_IsStarted = false;
        private bool CountDownwithBar_IsPaused = false;
        private DateTime CountDownwithBar_StartDateTime;
        private DateTime CountDownwithBar_EndDateTime;
        private DateTime CountDownwithBar_PauseDateTime;
        private void CountDownwithBar(IplImage drawing)
        {
            //loop
            var color = new CvScalar(To8bitMap(frameRatingSinAbs(10)), To8bitMap(frameRatingSinAbs(15)), 0x80);
            Cv.Set(drawing, new CvScalar(0,0,0));

            int x = 0;
            if (CountDownwithBar_IsStarted)
            {
                if (CountDownwithBar_EndDateTime > DateTime.Now)
                {
                    //pause
                    if (CountDownwithBar_IsPaused)
                    {
                        var diff = DateTime.Now.Ticks - CountDownwithBar_PauseDateTime.Ticks;
                        CountDownwithBar_StartDateTime = new DateTime(CountDownwithBar_StartDateTime.Ticks + diff);
                        CountDownwithBar_EndDateTime = new DateTime(CountDownwithBar_EndDateTime.Ticks + diff);
                        CountDownwithBar_PauseDateTime = DateTime.Now;

                        Cv.Copy(oldDrawing, drawing);
                    }
                    else
                    {
                        //count down
                        double diff = (double)((CountDownwithBar_EndDateTime - DateTime.Now).Ticks) / (double)((CountDownwithBar_EndDateTime - CountDownwithBar_StartDateTime).Ticks);
                        x = (int)((1.0 - diff) * FPGASender.ALL_WIDTH);
                        Cv.DrawRect(drawing, x, 0, FPGASender.ALL_WIDTH, FPGASender.ALL_HEIGHT, color, -1);
                        Cv.CvtColor(drawing, drawing, ColorConversion.HsvToRgb);

                        var text = (CountDownwithBar_EndDateTime - DateTime.Now).ToString(@"mm\:ss");
                        var point = new CvPoint(0, (int)(FPGASender.ALL_HEIGHT * 0.8));
                        var font = text.Length > 4 ? new CvFont(FontFace.HersheyPlain, 0.1, 2.75, 0, 2, LineType.Link8) : new CvFont(FontFace.HersheyPlain, 0.7, 3, 0, 2, LineType.AntiAlias);
                        var tcolor = new CvColor(0xff, 0xff, 0xff);

                        Cv.PutText(drawing, text, point, font, tcolor);
                    }
                }
                else
                {
                    //end
                    var point1 = new CvPoint(0, (int)(FPGASender.ALL_HEIGHT * 0.5));
                    var point2 = new CvPoint((int)(FPGASender.ALL_WIDTH * 0.48), (int)(FPGASender.ALL_HEIGHT * 0.9));
                    var font = new CvFont(FontFace.HersheyPlain, 0.1, 2.5, 0, 2, LineType.Link8);
                    var tcolor = new CvColor(0xff, 0xff, 0xff);

                    Cv.PutText(drawing, "Time", point1, font, tcolor);
                    Cv.PutText(drawing, "up!", point2, font, tcolor);
                }
            }
            else
            {
                //ready
                Cv.DrawRect(drawing, x, 0, FPGASender.ALL_WIDTH, FPGASender.ALL_HEIGHT, color, -1);
                Cv.CvtColor(drawing, drawing, ColorConversion.HsvToRgb);

                var text = "Ready...";
                var point = new CvPoint(0, (int)(FPGASender.ALL_HEIGHT * 0.8));
                var font =new CvFont(FontFace.HersheyPlain, 0.1, 2.5, 0, 2, LineType.Link8);
                var tcolor = new CvColor(0xff, 0xff, 0xff);

                Cv.PutText(drawing, text, point, font, tcolor);
            }
        }
        private void XCountDownwithBarStart_Click(object sender, RoutedEventArgs e)
        {
            var span = new TimeSpan(0, XCountDownwithBarMinutes.Value, XCountDownwithBarSeconds.Value);
            CountDownwithBar_StartDateTime = DateTime.Now;
            CountDownwithBar_EndDateTime = new DateTime(DateTime.Now.Ticks + span.Ticks);
            CountDownwithBar_IsStarted = true;

            XCountDownwithBarStart.IsEnabled = true;
            XCountDownwithBarPauseResume.IsEnabled = true;
            XCountDownwithBarReset.IsEnabled = true;
        }
        private void XCountDownwithBarPauseResume_Click(object sender, RoutedEventArgs e)
        {
            CountDownwithBar_PauseDateTime = DateTime.Now;
            CountDownwithBar_IsPaused = !CountDownwithBar_IsPaused;

            XCountDownwithBarStart.IsEnabled = true;
            XCountDownwithBarPauseResume.IsEnabled = true;
            XCountDownwithBarReset.IsEnabled = true;
        }
        private void XCountDownwithBarReset_Click(object sender, RoutedEventArgs e)
        {
            CountDownwithBar_IsStarted = false;

            XCountDownwithBarStart.IsEnabled = true;
            XCountDownwithBarPauseResume.IsEnabled = true;
            XCountDownwithBarReset.IsEnabled = true;
        }
        private void RectTurn(IplImage drawing)
        {
            Cv.Set(drawing, new CvScalar(0,0,0));

            int size = (FPGASender.ALL_HEIGHT / 3);
            int x1 = (int)((FPGASender.ALL_WIDTH / 2.0)  - size);
            int x2 = (int)((FPGASender.ALL_WIDTH / 2.0)  + size);
            int y1 = (int)((FPGASender.ALL_HEIGHT / 2.0) - size);
            int y2 = (int)((FPGASender.ALL_HEIGHT / 2.0) + size);

            Cv.DrawRect(drawing,x1,y1,x2,y2,new CvScalar(frameRatingSin(8) * 360, 0xff, 200),-1,LineType.AntiAlias);
            Cv.CvtColor(drawing, drawing, ColorConversion.HsvToBgr);
            RotateImage(drawing, 2 * Math.PI * frameRatingSaw);
        }
        private static void RotateImage(IplImage img, double angle)
        {
            using (IplImage tmp = new IplImage(img.Size, BitDepth.U8, 3))
            {
                float[] m = new float[6];

                m[0] = (float)(Math.Cos(angle));
                m[1] = (float)(-Math.Sin(angle));
                m[2] = (float)(img.Width * 0.5);
                m[3] = -m[1];
                m[4] = m[0];
                m[5] = (float)(img.Height * 0.5);

                CvMat M = new CvMat(2, 3, MatrixType.F32C1, m);
                Cv.GetQuadrangleSubPix(img, tmp, M);

                Cv.Copy(tmp, img);
            }
        }
        private volatile IplImage sohgikenLogo_Mask = null;
        private void SohgikenLogo(IplImage drawing)
        {
            Cv.Set(drawing, new CvColor(0, 0, 0));
            if (sohgikenLogo_Mask == null)
            {
                using (var tmpMask = new IplImage(@"sohgiken.bmp", LoadMode.GrayScale))
                {
                    sohgikenLogo_Mask = new IplImage(drawing.Size, BitDepth.U8, 1);
                    Cv.Threshold(tmpMask, tmpMask, 0x80, 0xff, ThresholdType.Binary);
                    Cv.Resize(tmpMask, sohgikenLogo_Mask);
                }
            }
            using (var tmp = new IplImage(drawing.Size, BitDepth.U8, 3))
            {
                Cv.Set(tmp, new CvScalar(To8bitMap(frameRatingSinAbs(20)), To8bitMap(frameRatingSinAbs(30)), 0x80));
                Cv.CvtColor(tmp, tmp, ColorConversion.HsvToRgb);

                Cv.Copy(tmp, drawing, sohgikenLogo_Mask);
            }
        }

    }
}
