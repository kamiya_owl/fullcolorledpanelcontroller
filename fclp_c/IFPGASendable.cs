﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace fclp_c
{
	public interface IFPGASendable
	{
		bool Initialize();
		void Stop();
		bool Finalize();

		FPGASender.SendFromIplImageDelegate SendFromIplImage { get; set; }
		FPGASender.NotifyMessageDelegate NotifyMessage { get; set; }
	}
}
