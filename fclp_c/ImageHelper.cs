﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using OpenCvSharp;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Windows;
using System.Windows.Controls;
using fclp_c.FontChooser;
using System.IO;

namespace fclp_c
{
	class ImageHelper
	{
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//Convert
		/// <summary>
		/// GDI+Bitmapから特定位置を抜き出したIplImageを生成します
		/// </summary>
		/// <param name="origin_bitmap">元の画像</param>
		/// <param name="x">左上のx座標</param>
		/// <param name="y">左上のy座標</param>
		/// <returns>抜き出した画像、開放は呼び出し元が行なってください</returns>
		public static IplImage CropBitmapImageToIplImage(Bitmap origin_bitmap, int x, int y)
		{
			//ロック
			BitmapData data = origin_bitmap.LockBits(new Rectangle(x, y, FPGASender.ALL_WIDTH, FPGASender.ALL_HEIGHT), System.Drawing.Imaging.ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format24bppRgb);

			IplImage crop_image = new IplImage(FPGASender.ALL_WIDTH, FPGASender.ALL_HEIGHT, BitDepth.U8, 3);
			crop_image.ImageData = data.Scan0;
			/*
			unsafe
			{
				for (int i = 0; i < FPGASender.ALL_RESULUTION * FPGASender.PANEL_CHANNELS; ++i)
					((byte*)crop_image.ImageData)[i] = ((byte*)data.Scan0)[i];
			}
			 * */
			//ロック解除
			origin_bitmap.UnlockBits(data);

			return crop_image;
		}
		/// <summary>
		/// BitmapSourceをBitmapに変換します
		/// </summary>
		/// <typeparam name="TEncoder"></typeparam>
		/// <param name="src"></param>
		/// <returns></returns>
		public static Bitmap ToBitmap<TEncoder>(BitmapSource src) where TEncoder : BitmapEncoder, new()
		{
			//TODO:上下反転する可能性
			// エンコーダを作成してフレーム追加
			var encoder = new TEncoder();
			encoder.Frames.Add(BitmapFrame.Create(src));

			// ストリームを介して Bitmap に変換
			Bitmap dest = null;
			using (var s = new MemoryStream())
			{
				encoder.Save(s);
				s.Seek(0, SeekOrigin.Begin);
				using (var temp = new Bitmap(s))
				{
					// ストリームを閉じた後の Save メソッド呼び出し等で
					// GDI+例外が発生しないように、別の Bitmap へコピー
					dest = new Bitmap(temp);
				}
			}

			return dest;
		}
		public static Bitmap ToBitmap(BitmapSource src)
		{
			// フォーマットが異なるならば変換
			BitmapSource s = src;
			if (s.Format != PixelFormats.Rgb24)
			{
				s = new FormatConvertedBitmap(
					s,
					PixelFormats.Rgb24,
					null,
					0);
				s.Freeze();
			}
			// ピクセルデータをコピー
			int width = (int)s.Width;
			int height = (int)s.Height;
			int stride = width * 4;
			byte[] datas = new byte[stride * height];
			s.CopyPixels(datas, stride, 0);
			// Bitmapに書き出し
			Bitmap dest = new Bitmap(width, height, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
			System.Drawing.Imaging.BitmapData destBits = null;
			try
			{
				destBits = dest.LockBits(new System.Drawing.Rectangle(0, 0, width, height), System.Drawing.Imaging.ImageLockMode.WriteOnly, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
				Marshal.Copy(datas, 0, destBits.Scan0, datas.Length);
			}
			catch
			{
				dest.Dispose();
				dest = null;
				throw;
			}
			finally
			{
				if (dest != null && destBits != null)
				{
					dest.UnlockBits(destBits);
					//上下反転を修正
					//dest.RotateFlip(RotateFlipType.Rotate180FlipX);
				}
			}
			//dest.Dispose();
			return dest;
		}
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//DrawString(Graphics)

		/// <summary>
		/// LEDPanelにスクロールさせて表示させるための余白を持った画像を生成します。
		/// </summary>
		/// <param name="message">描画する文字列</param>
		/// 
		/// <param name="fontBrush">文字列のブラシ</param>
		/// <param name="drawFont">文字列のフォント</param>
		/// <returns>
		/// 文字列を描画した画像、開放は呼び出し元が行なってください
		/// </returns>
		public static Bitmap DrawStringForLEDPanel(string message, System.Drawing.Brush fontBrush, Font drawFont)
		{
			//画像サイズを取得
			int width = GetTextSize(message, fontBrush, drawFont);
			Console.WriteLine("width = {0}", width);
			//(FPGASender.ALL_WIDTH * 2) = 余白
			Bitmap bitmap = new Bitmap((int)(width + (FPGASender.ALL_WIDTH * 2)), (int)drawFont.GetHeight(), System.Drawing.Imaging.PixelFormat.Format24bppRgb);
			Graphics graphics = Graphics.FromImage(bitmap);
			//描画
			//graphics.DrawRectangle(new System.Drawing.Pen(backgroundBrush), 0, 0, bitmap.Width, bitmap.Height);//塗りつぶし
			graphics.DrawString(message, drawFont, fontBrush, new System.Drawing.Point(FPGASender.ALL_WIDTH, 0));
			graphics.Dispose();

			return bitmap;
		}
		/// <summary>
		/// LEDPanelにスクロールさせて表示させるための余白を持った画像を生成します。
		/// </summary>
		/// <param name="message">描画する文字列</param>
		/// 
		/// <param name="fontBrush">文字列のブラシ</param>
		/// <param name="fontname">描画するフォントファミリーの名前</param>
		/// <returns>
		/// 文字列を描画した画像、開放は呼び出し元が行なってください
		/// </returns>
		public static Bitmap DrawStringForLEDPanel(string message, System.Drawing.Brush fontBrush, string fontname = "メイリオ")
		{
			return DrawStringForLEDPanel(message, fontBrush, new Font(fontname, FPGASender.ALL_HEIGHT - 4));
		}
		/// <summary>
		/// LEDPanelにスクロールさせて表示させるための余白を持った画像を生成します。
		/// </summary>
		/// <param name="message">描画する文字列</param>
		/// 
		/// <param name="fontColor">文字色</param>
		/// <param name="fontname">文字のフォントの名前</param>
		/// <returns>
		/// 文字列を描画した画像、開放は呼び出し元が行なってください
		/// </returns>
		public static Bitmap DrawStringForLEDPanel(string message, System.Drawing.Color fontColor, string fontname = "メイリオ")
		{
			return DrawStringForLEDPanel(message, new System.Drawing.SolidBrush(fontColor), fontname);
		}
		/// <summary>
		/// LEDPanelにスクロールさせて表示させるための余白を持った画像を生成します。
		/// </summary>
		/// <param name="message">描画する文字列</param>
		/// 
		/// <param name="fontColor">文字色</param>
		/// <param name="fontname">文字のフォントの名前</param>
		/// <returns>
		/// 文字列を描画した画像、開放は呼び出し元が行なってください
		/// </returns>
		public static Bitmap DrawStringForLEDPanel(string message, int r, int g, int b, string fontname = "メイリオ")
		{
			return DrawStringForLEDPanel(message, new System.Drawing.SolidBrush(System.Drawing.Color.FromArgb(r, g, b)), fontname);
		}
		/// <summary>
		/// LEDPanelにスクロールさせて表示させるための余白を持った画像を生成します。
		/// </summary>
		/// <param name="message">描画する文字列</param>
		/// 
		/// <param name="fontColor">文字色</param>
		/// <param name="fontname">文字のフォントの名前</param>
		/// <returns>
		/// 文字列を描画した画像、開放は呼び出し元が行なってください
		/// </returns>
		public static Bitmap DrawStringForLEDPanel(string message, double r, double g, double b, string fontname = "メイリオ")
		{
			return DrawStringForLEDPanel(message, new System.Drawing.SolidBrush(System.Drawing.Color.FromArgb((int)r,(int) g,(int) b)), fontname);
		}

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//文字列の画像サイズを取得
		//TODO:ちゃんと取得できるようにする
		public static int GetTextSize(string message, System.Drawing.Brush fontBrush, Font drawFont)
		{
			//画像サイズを取得
			Bitmap bitmap = new Bitmap((int)((message.Length * drawFont.GetHeight())), (int)drawFont.GetHeight(), System.Drawing.Imaging.PixelFormat.Format24bppRgb);
			Graphics graphics = Graphics.FromImage(bitmap);
			//描画
			graphics.DrawString(message, drawFont, fontBrush, new System.Drawing.Point(FPGASender.ALL_WIDTH, 0));
			graphics.Dispose();

			bitmap.Save("gettextsize.bmp");
			BitmapData bitmapData = new BitmapData();
			bitmapData = bitmap.LockBits(new Rectangle(0,0,bitmap.Width,bitmap.Height),ImageLockMode.ReadOnly,System.Drawing.Imaging.PixelFormat.Format24bppRgb);
			//値を設定
			int i, j,c;
			bool finish = false;
			unsafe
			{
				//ポインタを取得
				byte* pointer = (byte*)bitmapData.Scan0;
				//右端から操作する
				for (i = bitmapData.Width; (!finish) && (i > 0) ; --i)
				{
					//縦向きに捜査をする
					for (j = 0; (!finish) && (j < bitmap.Height); ++j)
					{
						//24bpprgb
						for (c = 0;(!finish) && ( c < 3); ++c)
						{
							//Console.WriteLine("i = {0} j = {1} pointer = {2} data = {3}", i, j, i + (j * bitmapData.Width), *(pointer + i + (j * bitmapData.Width)));
							if (*(pointer + (i * 3) + (j * bitmapData.Width * 3) + c) != 0)
							{
								finish = true;
								break;
							}
						}
					}
				}
				//iが書かれてる右端だから一をたす
				++i;
			}
			//開放
			bitmap.UnlockBits(bitmapData);
			bitmapData = null;
			bitmap.Dispose();
			bitmap = null;

			//調整
			if (i < drawFont.Height) i = drawFont.Height;
			return i;
		}
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//画像サイズ補完
		public static BitmapSource FixImageSize(BitmapSource source, System.Windows.Media.Brush fixBrush, int fix_width, int fix_height)
		{
			//修正するサイズを取得
			int fix_x = (int)(fix_width - source.Width);
			int fix_y = (int)(fix_height - source.PixelHeight);

			DrawingVisual dv = new DrawingVisual();
			Rect rect;
			//!!!!!---DrawiContextを閉じないと画像がnullになるので注意---!!!!!
			using (DrawingContext dc = dv.RenderOpen())
			{
				if (fix_x > 0)
				{
					//x方向補完
					rect = new Rect(source.Width, 0, fix_width, fix_height);
					dc.DrawRectangle(fixBrush, null, rect);
				}
				else if (fix_y > 0)
				{
					//y方向補完
					rect = new Rect(0, source.Height, fix_width, fix_height);
					dc.DrawRectangle(fixBrush, null, rect);
				}
				else
				{
					//何も起こりません
					return source;
				}
			}
			var bitmap = new RenderTargetBitmap(fix_width, fix_height, 96.0, 96.0, System.Windows.Media.PixelFormats.Default);
			bitmap.Render(dv);
			bitmap.Freeze();
			//bitmapに変換
			var dest = new FormatConvertedBitmap(bitmap, PixelFormats.Rgb24, null, 0);//RGB24に変換
			dest.Freeze();
			return dest;
		}
		public static BitmapSource FixImageSize(BitmapSource source)
		{
			return FixImageSize(source, System.Windows.Media.Brushes.Black,(int)source.Width,FPGASender.ALL_HEIGHT);
		}




		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//DrawString(旧ImageHelper)
		public static BitmapSource CreateBitmapSourceFromText(FormattedText formattedText, System.Windows.Media.Brush background, int width_rate = 2)
		{
			//画像サイズは余剰分をとっておく
			if (width_rate < 1) width_rate = 2;
			int width = (int)formattedText.WidthIncludingTrailingWhitespace + FPGASender.ALL_WIDTH * 2;//余白
			int height = FPGASender.ALL_HEIGHT;//(int)formattedText.Height;//
			//画像サイズの横幅が小さ過ぎないように
			//if (width < ALL_PANEL_WIDTH) width = ALL_PANEL_WIDTH;
			//if (height < ALL_PANEL_HEIGHT) height = ALL_PANEL_HEIGHT;
			DrawingVisual dv = new DrawingVisual();
			//!!!!!---DrawiContextを閉じないと画像がnullになるので注意---!!!!!
			using (DrawingContext dc = dv.RenderOpen())
			{
				//imagesourceを書き込む
				var rect = new Rect(new System.Windows.Point(0, 0), new System.Windows.Point(width, height));
				dc.DrawRectangle(background, null, rect);
				dc.DrawText(formattedText, new System.Windows.Point(FPGASender.ALL_WIDTH, 0));
			}
			var bitmap = new RenderTargetBitmap(width, height, 96.0, 96.0, System.Windows.Media.PixelFormats.Default);
			bitmap.Render(dv);
			bitmap.Freeze();
			//bitmapに変換
			var dest = new FormatConvertedBitmap(bitmap, PixelFormats.Rgb24, null, 0);
			dest.Freeze();
			return dest;
		}

		public static BitmapSource CreateBitmapSourceFromText(string message, System.Windows.Media.Brush textBlush, System.Windows.Media.Brush backgroundBlush, System.Windows.Media.FontFamily f_family, double f_size, System.Windows.FontStyle f_style, FontStretch f_stretch, System.Windows.FontWeight f_weight, int width_rate = 2)
		{
			//フォント付きテキストを生成
			FormattedText formattedText = new FormattedText(message,
															System.Threading.Thread.CurrentThread.CurrentCulture,
															System.Windows.FlowDirection.LeftToRight,
															new Typeface(f_family, f_style, f_weight, f_stretch), f_size, textBlush);
			return CreateBitmapSourceFromText(formattedText, backgroundBlush, width_rate);
		}
		public static BitmapSource CreateBitmapSourceFromText(string message, System.Windows.Media.Brush textBlush, System.Windows.Media.Brush backgroundBlush, double f_size = 28, int width_rate = 2)
		{
			return CreateBitmapSourceFromText(message, textBlush, backgroundBlush, new System.Windows.Media.FontFamily(), f_size, new System.Windows.FontStyle(), new FontStretch(), new System.Windows.FontWeight(), width_rate);
		}
		public static BitmapSource CreateBitmapSourceFromText(string message, FontChooser.FontChooser fontChooser, System.Windows.Media.Brush textBlush, System.Windows.Media.Brush backgroundBlush, System.Windows.Media.Brush penBlush, int width_rate = 2)
		{
			//テキストブロックを作って適用させ、そこからプロパティを取得する
			TextBlock block = new TextBlock();
			fontChooser.ApplyPropertiesToObject(block);
			//はいはいオーバーロード
			return CreateBitmapSourceFromText(message, textBlush, backgroundBlush, block.FontFamily, block.FontSize, block.FontStyle, block.FontStretch, block.FontWeight, width_rate);
		}


	}
}


