﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using OpenCvSharp;
using System.Drawing;
using System.Threading.Tasks;
using System.IO;

namespace fclp_c
{
	/// <summary>
	/// TestPatternWindow.xaml の相互作用ロジック
	/// </summary>
	public partial class TestPatternWindow : Window,IFPGASendable
	{
		//delegate
		public FPGASender.SendFromIplImageDelegate SendFromIplImage { get; set; }
		public FPGASender.NotifyMessageDelegate NotifyMessage { get; set; }
		//thread
		int interval = 20;//Tick[ms]
		System.Threading.Timer _drawingThread = null;
		DispatcherTimer _updateThread = null;
		//drawing
		volatile IplImage _drawingImage = null;//こいつを書き換えてもらう
		enum Algorithm { TestGradient, WaveGradient, TestTextScroll, DotMove };
		volatile Algorithm _algorithm = Algorithm.TestGradient;
		//preview
		CvWindow _previewWindow = null;
		//gradient
		volatile int _counter = 0;
		volatile int _downCounter = 255;
		volatile int _colorFaze = 0;
		//wave
		volatile int _column = 0;
		volatile int _columnSpeed = 2;//横の進み
		//testText
		volatile string _testText = "Windows でコンピューターの世界が広がります。1234567890";
		volatile Bitmap _baseBitmapTextImage = null;
		//dotmove
		volatile int _row = 0;

		public TestPatternWindow()
		{
			InitializeComponent();
		}
		//////////////////////////////////////////////////////////////////////////////////////////////
		//Interface
		public bool Initialize()
		{
			//SendFromIplImage += new FPGASender.SendFromIplImageDelegate(ShowPreview);
			_previewWindow = new CvWindow("Preview", WindowMode.FreeRatio);

			//Init
			if (_drawingImage == null)
			{
				_drawingImage = new IplImage(FPGASender.ALL_WIDTH, FPGASender.ALL_HEIGHT, BitDepth.U8, 3);
				//TestGradient
				_counter = 0;
				_downCounter = 255;
				_colorFaze = 0;
				//WaveGradient
				_column = 0;
				//dotmove
				_row = 0;
			}
			//スレッドの開始
			InitializeThread();

			return true;
		}
/*		public bool ShowPreview(IplImage preview_image)
		{

			return true;
		}*/

		public void Stop()
		{
			FinalizeThread();
		}

		public bool Finalize()
		{
			FinalizeThread();
			_previewWindow.Dispose();
			_previewWindow = null;

			return true;
		}

		//////////////////////////////////////////////////////////////////////////////////////////////
		//Thread
		private void InitializeThread()
		{
			FinalizeThread();

			_drawingThread = new System.Threading.Timer(new System.Threading.TimerCallback(DrawingThreadProcess), null, interval, interval);
			
			_updateThread = new DispatcherTimer();
			_updateThread.Interval = new TimeSpan(0, 0, 0, 0, FPGASender.MASTER_INTERVAL);//！！！固定されている
			_updateThread.Tick += new EventHandler(_updateThread_Tick);
			_updateThread.Start();
			 
			
		}
		private void ChangeThreadInterval()
		{
			if (_drawingThread != null)
			{
				_drawingThread.Change(interval, interval);
			}
			if (_updateThread != null)
			{
				//こっちは固定しておく
				//_updateThread.Stop();
				//_updateThread.Interval = new TimeSpan(0, 0, 0, 0, interval);
				//_updateThread.Start();
			}
			 
		}
		private void FinalizeThread()
		{
			if (_drawingThread != null)
			{
				_drawingThread.Dispose();
				_drawingThread = null;
			}
			
			if (_updateThread != null)
			{
				if (_updateThread.IsEnabled) _updateThread.Stop();
				_updateThread = null;
			}
			 
		}
		//DispatcherTimer
		void _updateThread_Tick(object sender, EventArgs e)
		{
			//チェックボックスで描画停止
			if (XIsEnableCheckBox.IsChecked == false) return;
			if (_drawingImage == null) return;
			if (_drawingImage.IsDisposed) return;
			//コピー
			IplImage copy_image = Cv.CloneImage(_drawingImage);
			//送信
			SendFromIplImage(copy_image,false);
			//プレビュー
			//if (_previewWindow != null) _previewWindow.ShowImage(copy_image);

			//開放
			copy_image.Dispose();
			copy_image = null;
		}
		//Timer
		void DrawingThreadProcess(object args)
		{
			//分岐
			switch (_algorithm)
			{
				case Algorithm.TestGradient:
					GradientTest();
					break;
				case Algorithm.WaveGradient:
					WaveGradientTest();
					break;
				case Algorithm.TestTextScroll:
					//TODO:ちゃんと使えるようにする
					TestTextScroll();
					break;
				case Algorithm.DotMove:
					DotMoveDraw();
					break;
			}
			//プレビュー
			if((_previewWindow != null)&&(_drawingImage != null)) _previewWindow.ShowImage(_drawingImage);
		}

		//////////////////////////////////////////////////////////////////////////////////////////////
		//Drawing
		private CvScalar CvScalar_RGB(int r, int g, int b)
		{
			//BGR->RGBで指定できる
			return new CvScalar(b, g, r);
		}
		private void GradientTest()
		{
			if (_drawingImage == null) _drawingImage = new IplImage(FPGASender.ALL_WIDTH, FPGASender.ALL_HEIGHT, BitDepth.U8, 3);
			//Drawing
			_counter += 9;//速度を決定できる
			_downCounter = 255 - _counter;
			switch(_colorFaze)
			{
				case 0://black -> red
					Cv.Rectangle(_drawingImage, new CvPoint(0, 0), new CvPoint(FPGASender.ALL_WIDTH, FPGASender.ALL_HEIGHT), CvScalar_RGB(_counter, 0, 0), Cv.FILLED);
					break;
				case 1://red -> green
					Cv.Rectangle(_drawingImage, new CvPoint(0, 0), new CvPoint(FPGASender.ALL_WIDTH, FPGASender.ALL_HEIGHT), CvScalar_RGB(_downCounter, _counter, 0), Cv.FILLED);
					break;
				case 2://green -> blue
					Cv.Rectangle(_drawingImage, new CvPoint(0, 0), new CvPoint(FPGASender.ALL_WIDTH, FPGASender.ALL_HEIGHT), CvScalar_RGB(0, _downCounter, _counter), Cv.FILLED);
					break;
				case 3://blue -> cyan
					Cv.Rectangle(_drawingImage, new CvPoint(0, 0), new CvPoint(FPGASender.ALL_WIDTH, FPGASender.ALL_HEIGHT), CvScalar_RGB(0, _counter, 255), Cv.FILLED);
					break;
				case 4://cyan -> magenta
					Cv.Rectangle(_drawingImage, new CvPoint(0, 0), new CvPoint(FPGASender.ALL_WIDTH, FPGASender.ALL_HEIGHT), CvScalar_RGB(_counter, _downCounter, 255), Cv.FILLED);
					break;
				case 5://magenta -> yellow
					Cv.Rectangle(_drawingImage, new CvPoint(0, 0), new CvPoint(FPGASender.ALL_WIDTH, FPGASender.ALL_HEIGHT), CvScalar_RGB(255, _counter, _downCounter), Cv.FILLED);
					break;
				case 6://yellow -> white
					Cv.Rectangle(_drawingImage, new CvPoint(0, 0), new CvPoint(FPGASender.ALL_WIDTH, FPGASender.ALL_HEIGHT), CvScalar_RGB(255, 255, _counter), Cv.FILLED);
					break;
				case 7://white -> black
					Cv.Rectangle(_drawingImage, new CvPoint(0, 0), new CvPoint(FPGASender.ALL_WIDTH, FPGASender.ALL_HEIGHT), CvScalar_RGB(_downCounter, _downCounter, _downCounter), Cv.FILLED);
					break;
				//if add...

				default:
					//reset
					_counter = 0;
					_colorFaze = 0;
					//black
					Cv.Rectangle(_drawingImage, new CvPoint(0, 0), new CvPoint(FPGASender.ALL_WIDTH, FPGASender.ALL_HEIGHT), CvScalar_RGB(0, 0, 0), Cv.FILLED);
					break;
			}
			if (_counter > 254)
			{
				//フェーズ変更
				_counter = 0;
				++_colorFaze;
			}
		}
		private void WaveGradientTest()
		{
			if (_drawingImage == null) _drawingImage = new IplImage(FPGASender.ALL_WIDTH, FPGASender.ALL_HEIGHT, BitDepth.U8, 3);
			//Drawing
			switch (_colorFaze)
			{
				case 0://black -> red
					Cv.Rectangle(_drawingImage, new CvPoint(_column, 0), new CvPoint(_column + _columnSpeed, FPGASender.ALL_HEIGHT), CvScalar_RGB(_counter, 0, 0),Cv.FILLED);
					break;
				case 1://red -> green
					Cv.Rectangle(_drawingImage, new CvPoint(_column, 0), new CvPoint(_column + _columnSpeed, FPGASender.ALL_HEIGHT), CvScalar_RGB(_downCounter, _counter, 0), Cv.FILLED);
					break;
				case 2://green -> blue
					Cv.Rectangle(_drawingImage, new CvPoint(_column, 0), new CvPoint(_column + _columnSpeed, FPGASender.ALL_HEIGHT), CvScalar_RGB(0, _downCounter, _counter), Cv.FILLED);
					break;
				case 3://blue -> cyan
					Cv.Rectangle(_drawingImage, new CvPoint(_column, 0), new CvPoint(_column + _columnSpeed, FPGASender.ALL_HEIGHT), CvScalar_RGB(0, _counter, 255), Cv.FILLED);
					break;
				case 4://cyan -> magenta
					Cv.Rectangle(_drawingImage, new CvPoint(_column, 0), new CvPoint(_column + _columnSpeed, FPGASender.ALL_HEIGHT), CvScalar_RGB(_counter, _downCounter, 255), Cv.FILLED);
					break;
				case 5://magenta -> yellow
					Cv.Rectangle(_drawingImage, new CvPoint(_column, 0), new CvPoint(_column + _columnSpeed, FPGASender.ALL_HEIGHT), CvScalar_RGB(255, _counter, _downCounter), Cv.FILLED);
					break;
				case 6://yellow -> white
					Cv.Rectangle(_drawingImage, new CvPoint(_column, 0), new CvPoint(_column + _columnSpeed, FPGASender.ALL_HEIGHT), CvScalar_RGB(255, 255, _counter), Cv.FILLED);
					break;
				case 7://white -> black
					Cv.Rectangle(_drawingImage, new CvPoint(_column, 0), new CvPoint(_column + _columnSpeed, FPGASender.ALL_HEIGHT), CvScalar_RGB(_downCounter, _downCounter, _downCounter), Cv.FILLED);
					break;
				//if add...

				default:
					//reset
					_counter = 0;
					_downCounter = 255 - _counter;//最初にインクリしない代わり
					_colorFaze = 0;
					//black
					Cv.Rectangle(_drawingImage, new CvPoint(_column, 0), new CvPoint(_column + _columnSpeed, FPGASender.ALL_HEIGHT), CvScalar_RGB(0, 0, 0), Cv.FILLED);
					break;
			}
			//描画する行
			if (_column >= FPGASender.ALL_WIDTH) _column = 0;//インクリされる
			else _column += _columnSpeed;
			//Drawing
			if (_counter ==  255)
			{
				//フェーズ変更
				_counter = 0;
				_downCounter = 255 - _counter;
				++_colorFaze;
			}
			else
			{
				++_counter;//速度を決定できる
				_downCounter = 255 - _counter;
			}
		}
		volatile bool _lockTestTextScroll = false;
		private void TestTextScroll()
		{
			if (_lockTestTextScroll) return;
			else _lockTestTextScroll = true;

			//if (_baseBitmapTextImage == null)
				//CreateTextImage();

			_lockTestTextScroll = false;
			//nullチェック
			if (_baseBitmapTextImage == null) return;
			//新規生成を行う
			if (_column >= (_baseBitmapTextImage.Width - FPGASender.ALL_WIDTH))//余白:# ####test####
				_column = 0;
			else ++_column;
			if (_drawingImage != null) _drawingImage.Dispose(); _drawingImage = null;
			_drawingImage = ImageHelper.CropBitmapImageToIplImage(_baseBitmapTextImage, _column, 0);
		}
		private bool CreateTextImage()
		{
			//neta
			LinearGradientBrush myLinearGradientBrush = new LinearGradientBrush();
			myLinearGradientBrush.StartPoint = new System.Windows.Point(0, 0);
			myLinearGradientBrush.EndPoint = new System.Windows.Point(1, 0);
			myLinearGradientBrush.GradientStops.Add(new GradientStop(Colors.Yellow, 0.0));
			myLinearGradientBrush.GradientStops.Add(new GradientStop(Colors.Red, 0.25));
			myLinearGradientBrush.GradientStops.Add(new GradientStop(Colors.Blue, 0.75));
			myLinearGradientBrush.GradientStops.Add(new GradientStop(Colors.LimeGreen, 1.0));
			//画像の生成
			var imageSource = ImageHelper.CreateBitmapSourceFromText(_testText, myLinearGradientBrush, new SolidColorBrush(Colors.Black), 32, 2);
			//縦幅の補完
			ImageHelper.FixImageSize(imageSource);
			Bitmap new_image = null;
			//変換
			try
			{
				//new_image = ImageHelper.ToBitmap(imageSource);
				new_image = ImageHelper.ToBitmap<BmpBitmapEncoder>(imageSource);
			}
			catch (Exception e)
			{
				Console.WriteLine("{0}\n{1}\n{2}", e.Source, e.Message, e.StackTrace);
				return false;
			}
			//参照の更新
			_baseBitmapTextImage = new_image;
			//開放
			imageSource = null;
			myLinearGradientBrush = null;

			//通知
			//NotifyMessage(this, "文字列\'{0}\'を描画した画像を生成しました。", _testText);

			GC.Collect();

			return true;
		}
		private void DotMoveDraw()
		{
			if (_drawingImage == null) _drawingImage = new IplImage(FPGASender.ALL_WIDTH, FPGASender.ALL_HEIGHT, BitDepth.U8, 3);
			//Drawing
			//描画するドットの決定
			UpdateRowAndColumn();
			switch (_colorFaze)
			{
				case 0://black -> red
					Cv.Rectangle(_drawingImage, new CvPoint(_column, _row), new CvPoint(_column, _row), CvScalar_RGB(_counter, 0, 0), Cv.FILLED);
					break;
				case 1://red -> green
					Cv.Rectangle(_drawingImage, new CvPoint(_column, _row), new CvPoint(_column, _row), CvScalar_RGB(_downCounter, _counter, 0), Cv.FILLED);
					break;
				case 2://green -> blue
					Cv.Rectangle(_drawingImage, new CvPoint(_column, _row), new CvPoint(_column, _row), CvScalar_RGB(0, _downCounter, _counter), Cv.FILLED);
					break;
				case 3://blue -> cyan
					Cv.Rectangle(_drawingImage, new CvPoint(_column, _row), new CvPoint(_column, _row), CvScalar_RGB(0, _counter, 255), Cv.FILLED);
					break;
				case 4://cyan -> magenta
					Cv.Rectangle(_drawingImage, new CvPoint(_column, _row), new CvPoint(_column, _row), CvScalar_RGB(_counter, _downCounter, 255), Cv.FILLED);
					break;
				case 5://magenta -> yellow
					Cv.Rectangle(_drawingImage, new CvPoint(_column, _row), new CvPoint(_column, _row), CvScalar_RGB(255, _counter, _downCounter), Cv.FILLED);
					break;
				case 6://yellow -> white
					Cv.Rectangle(_drawingImage, new CvPoint(_column, _row), new CvPoint(_column, _row), CvScalar_RGB(255, 255, _counter), Cv.FILLED);
					break;
				case 7://white -> black
					Cv.Rectangle(_drawingImage, new CvPoint(_column, _row), new CvPoint(_column, _row), CvScalar_RGB(_downCounter, _downCounter, _downCounter), Cv.FILLED);
					break;
				//if add...

				default:
					//reset
					_counter = 0;
					_downCounter = 255 - _counter;//最初にインクリしない代わり
					_colorFaze = 0;
					//black
					Cv.Rectangle(_drawingImage, new CvPoint(_column, _row), new CvPoint(_column, _row), CvScalar_RGB(0, 0, 0), Cv.FILLED);
					break;
			}
			//Drawing
			if (_counter == 255)
			{
				//フェーズ変更
				_counter = 0;
				_downCounter = 255 - _counter;
				++_colorFaze;
			}
			else
			{
				++_counter;//速度を決定できる
				_downCounter = 255 - _counter;
			}


		}
		private void ResetIsWrote()
		{
			for(int j = 0 ; j < _drawingImage.Height ; ++j)
				for (int i = 0; i < _drawingImage.Width; ++i)
					IsWrote[j,i] = false;
		}
		private void UpdateRowAndColumn()
		{
			Vector oldVector = _vector;
			//方向を決定
			switch(_vector)
			{
				case Vector.Right:
					++_column;
					if (_column == _drawingImage.Width - 1)
					{
						++_vector;
						break;
					}
					if (IsWrote[_row, _column + 1]) ++_vector;
					break;
				case Vector.Down:
					++_row;
					if (_row == _drawingImage.Height - 1)
					{
						++_vector;
						break;
					}
					if (IsWrote[_row + 1, _column]) ++_vector;
					break;

				case Vector.Left:
					--_column;
					if (_column == 0)
					{
						++_vector;
						break;
					}
					if (IsWrote[_row, _column - 1]) ++_vector;
					break;
				case Vector.Up:
					--_row;
					if (_row == 0)
					{
						++_vector;
						break;
					}
					if (IsWrote[_row - 1, _column]) ++_vector;
					break;
				case Vector.Default:
					_vector = 0;
					break;
			}
			//現在いるところをチェック済みにする
			IsWrote[_row, _column] = true;
			if (oldVector != _vector) ++_cannotShiftCounter;
			else _cannotShiftCounter = 0;
			Random r = new Random();
			if (_cannotShiftCounter > r.Next(4, 10))//乱数を使うのは次のフェーズがどれになるかランダムにするため
			{
				_cannotShiftCounter = 0;
				ResetIsWrote();

			}

			//Console.WriteLine("_column = {0} , row = {1} , vector = {2} , image::width = {3} image::height = {4}", _column, _row, _vector, _drawingImage.Width, _drawingImage.Height);
		}
		enum Vector { Right = 0, Down , Left , Up , Default };//defaultを最後に書く、それ以外は自由
		Vector _vector = Vector.Default;
		bool[,] IsWrote = null;
		int _cannotShiftCounter = 0;
		//////////////////////////////////////////////////////////////////////////////////////////////
		//Event
		private void XChangeIntervalButton_Click(object sender, RoutedEventArgs e)
		{
			ChangeThreadInterval();
			NotifyMessage(this, "スレッドを再生成しました。\nInterval = {0}[ms]", interval);
		}

		private void XIntervalSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
		{
			//Threadを再生成
			interval = (int)XIntervalSlider.Value;
			if(XIntervalText != null) XIntervalText.Content = interval;
		}

		private void XIsEnableCheckBox_Checked(object sender, RoutedEventArgs e)
		{
			NotifyMessage(this, "テストパターンの生成を開始しました。");
		}

		private void XIsEnableCheckBox_Unchecked(object sender, RoutedEventArgs e)
		{
			NotifyMessage(this, "テストパターンの生成を停止しました。");
		}
		//////////////////////////////////////////////////////////////////////////////////////////////
		//テスト内容の変更
		private void SetAlgorythm(Algorithm alg)
		{
			this._algorithm = alg;

			if(NotifyMessage != null) NotifyMessage(this, "テスト内容を変更しました");
		}
		private void XTestGradientRadioButton_Checked(object sender, RoutedEventArgs e)
		{
			SetAlgorythm(Algorithm.TestGradient);
		}

		private void XWaveGradientRadioButton_Checked(object sender, RoutedEventArgs e)
		{
			SetAlgorythm(Algorithm.WaveGradient);
		}

		private void XTestTextRadioButton_Checked(object sender, RoutedEventArgs e)
		{

			CreateTextImage();

			SetAlgorythm(Algorithm.TestTextScroll);
		}

		private void XArgsTextBox_TextChanged(object sender, TextChangedEventArgs e)
		{
			//this._testText = XArgsTextBox.Text;
		}

		private void XArgsTextBox_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.Key == Key.Enter)
			{
				this._testText = XArgsTextBox.Text;
				//一旦停止
				_drawingThread.Change(System.Threading.Timeout.Infinite, System.Threading.Timeout.Infinite);
				CreateTextImage();
				//再開
				_drawingThread.Change(100, interval);
			}
		}

		private void XDotMoveRadioButton_Checked(object sender, RoutedEventArgs e)
		{
			IsWrote = new bool[_drawingImage.Height,_drawingImage.Width];
			_vector = Vector.Default;//これ重要
			_column = 0;
			_row = 0;
			_cannotShiftCounter = 0;

			SetAlgorythm(Algorithm.DotMove);
		}

	}
}
