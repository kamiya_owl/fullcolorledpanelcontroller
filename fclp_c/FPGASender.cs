﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenCvSharp;
using System.Runtime.InteropServices;
using System.IO.Ports;
using System.IO;
using System.Threading.Tasks;

namespace fclp_c
{
	public class FPGASender
	{
		//定数値は全てここで決定
		public static int PANEL_WIDTH = 16;
		public static int PANEL_HEIGHT = 16;
		public static int PANEL_CHANNELS = 3;
		public static int ALL_ROW = 2;
		public static int ALL_COLUMN = 4;
		public static int MASTER_INTERVAL = 10;//[m/s]
		//変更された場合に再計算が必要
		public static int ALL_WIDTH = PANEL_WIDTH * ALL_COLUMN;
		public static int ALL_HEIGHT = PANEL_HEIGHT * ALL_ROW;
		public static int PANEL_RESOLUTION = PANEL_WIDTH * PANEL_HEIGHT;
		public static int PANEL_BUFFER_SIZE = PANEL_RESOLUTION * PANEL_CHANNELS + 1;
		public static int ALL_COUNT = ALL_ROW * ALL_COLUMN;
		public static int ALL_RESULUTION = PANEL_RESOLUTION * ALL_COUNT;
		public static int ALL_BUFFER_SIZE = PANEL_BUFFER_SIZE * ALL_COUNT;
		//
		//CvSplitのためだけに用意
		public const int R = 0;
		public const int G = 1;
		public const int B = 2;
		public const int A = 3;
		//SerialPort
		public static string PortName = "COM";
		public static int BaudRate = 2000000;
		public static Parity ParityBit = Parity.None;
		public static int DataBit = 8;
		public static StopBits StopBit = StopBits.One;
		//delegate
		public delegate bool SendFromIplImageDelegate(IplImage image,bool needFlipY);
		public delegate bool NotifyMessageDelegate(object sender, string message, params object[] args);

		public static void UpdateSettingValue()
		{
			//再計算を行う
			ALL_WIDTH = PANEL_WIDTH * ALL_COLUMN;
			ALL_HEIGHT = PANEL_HEIGHT * ALL_ROW;
			PANEL_RESOLUTION = PANEL_WIDTH * PANEL_HEIGHT;
			PANEL_BUFFER_SIZE = PANEL_RESOLUTION * PANEL_CHANNELS + 1;
			ALL_COUNT = ALL_ROW * ALL_COLUMN;
			ALL_RESULUTION = PANEL_RESOLUTION * ALL_COUNT;
			ALL_BUFFER_SIZE = PANEL_BUFFER_SIZE * ALL_COUNT;
		}

		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//送信、変換
		/// <summary>
		/// 指定された画像を変換して、送信を行う。
		/// </summary>
		/// <param name="serialport">送信するシリアルポート</param>
		/// <param name="origin_image">送信する画像</param>
		/// <returns>正しく変換し、送信できたかを示す真偽値</returns>
		public static bool Send(SerialPort serialport,IplImage origin_image)
		{
			byte[] buffer = new byte[ALL_BUFFER_SIZE];
			if (!ConvertFPGAFormat(origin_image, ref buffer)) return false;
			return SendFPGAFormatData(serialport, buffer);
		}
		/// <summary>
		/// 画像データを送信形式に変換する。
		/// </summary>
		/// <param name="origin_image">変換する元画像</param>
		/// <param name="buffer">送信するデータが格納された配列</param>
		/// <returns>変換が正しく行われたかを示す真偽値</returns>
		public static bool ConvertFPGAFormat(IplImage origin_image,ref byte[] buffer)//bufferの生成、破棄は呼び出し元で行う
		{
			//resize
			IplImage resize_image = new IplImage(ALL_WIDTH, ALL_HEIGHT, BitDepth.U8, PANEL_CHANNELS);
			Cv.Resize(origin_image, resize_image);

			//r g b initialize
			IplImage[] single_color_image = new IplImage[PANEL_CHANNELS];//色分割画像を保持
			for (int c = 0; c < PANEL_CHANNELS; ++c )
				single_color_image[c] = new IplImage(PANEL_WIDTH, PANEL_HEIGHT, BitDepth.U8, 1);
			//data copy
			for (int j = 0; j < ALL_ROW; ++j)
			{
				for (int i = 0; i < ALL_COLUMN; ++i)
				{
					//counter
					int counter = i + j * ALL_COLUMN;
					//offset
					int pixel_offset = counter * PANEL_BUFFER_SIZE;
					//ROI split
					Cv.SetImageROI(resize_image, new CvRect(i * PANEL_WIDTH, j * PANEL_HEIGHT, PANEL_WIDTH, PANEL_HEIGHT));
					//flip
					IplImage flip_image = new IplImage(PANEL_WIDTH,PANEL_HEIGHT,BitDepth.U8,PANEL_CHANNELS);
					Cv.Flip(resize_image, flip_image, FlipMode.X);
					//color split
					Cv.Split(flip_image, single_color_image[B], single_color_image[G], single_color_image[R], null);//Alphaは必要ない
					Cv.ResetImageROI(resize_image);
					//address
					buffer[pixel_offset] = (byte)counter;//もしかしたらキャストで問題が発生するかも
					//data move
					for (int c = 0; c < PANEL_CHANNELS; ++c)
					{
						//unsafe copy
						unsafe
						{
							for (int p = 0; p < PANEL_RESOLUTION; ++p)
							{
								buffer[(pixel_offset + 1) + (c * PANEL_RESOLUTION) + p] = ((byte*)(single_color_image[c].ImageData))[p];
							}
						}
					}
					//Release
					flip_image.Dispose();
					flip_image = null;
				}
			}
			//Release
			resize_image.Dispose();
			for (int c = 0; c < PANEL_CHANNELS; ++c)
				single_color_image[c].Dispose();

			return true;
		}
		/// <summary>
		/// 指定されたデータを送信する。
		/// </summary>
		/// <param name="serialport">送信するシリアルポート</param>
		/// <param name="buffer">送信するデータ</param>
		/// <returns>正しく送信できたかを示す真偽値</returns>
		public static bool SendFPGAFormatData(SerialPort serialport, byte[] buffer)
		{
			if (!serialport.IsOpen) return false;
			//TODO:非同期化
			serialport.Write(buffer, 0, buffer.Length);
			return true;

		}

		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//プレビュー関連
		public static IplImage PreviewResizeImage(IplImage send_image)
		{
			IplImage resize_image = new IplImage(FPGASender.ALL_WIDTH, FPGASender.ALL_HEIGHT, BitDepth.U8, 3);
			//送信イメージを設定されたサイズでリサイズ
			Cv.Resize(send_image, resize_image);

			return resize_image;
		}
		public static IplImage[] PreviewDivideImage(IplImage send_image)
		{
			//resize
			IplImage resize_image = new IplImage(ALL_WIDTH, ALL_HEIGHT, BitDepth.U8, PANEL_CHANNELS);
			Cv.Resize(send_image, resize_image);
			//divide initialize
			IplImage[] divide_images = new IplImage[ALL_COUNT];//分割画像を保持
			for (int d = 0; d < ALL_COUNT; ++d)
				divide_images[d] = new IplImage(PANEL_WIDTH, PANEL_HEIGHT, BitDepth.U8, PANEL_CHANNELS);

			for (int j = 0; j < ALL_ROW; ++j)
			{
				for (int i = 0; i < ALL_COLUMN; ++i)
				{
					//counter
					int counter = i + j * ALL_COLUMN;
					//ROI split
					Cv.SetImageROI(resize_image, new CvRect(i * PANEL_WIDTH, j * PANEL_HEIGHT, PANEL_WIDTH, PANEL_HEIGHT));

					//画像をコピー
					divide_images[counter] = Cv.CloneImage(resize_image);

					//ROIを解除
					Cv.ResetImageROI(resize_image);
				}
			}

			//Release
			resize_image.Dispose();
			resize_image = null;

			return divide_images;
		}
		public static void PreviewDivideImageDispose(IplImage[] divide_images)
		{
			for (int d = 0; d < ALL_COUNT; ++d)
			{
				divide_images[d].Dispose();
				divide_images[d] = null;
			}
		}
		public static CvWindow[] PreviewDivideWindowInitialize()
		{
			CvWindow[] divide_windows = new CvWindow[ALL_COUNT];
			for (int d = 0; d < ALL_COUNT; ++d)
				divide_windows[d] = new CvWindow(string.Format("ADDRESS = {0}", d), WindowMode.KeepRatio);
			return divide_windows;
		}
		public static void PreviewDivideWindowShowImage(IplImage[] images,CvWindow[] windows)
		{
			//パネル数が変えられた時の対策
			if (windows.Length > ALL_COUNT)
			{
				//再生成
				PreviewDivideWindowFinalize(windows);
				windows = PreviewDivideWindowInitialize();
			}
			for (int d = 0; d < ALL_COUNT; ++d)
				windows[d].ShowImage(images[d]);
		}
		public static void PreviewDivideWindowFinalize(CvWindow[] divide_windows)
		{
			for (int d = 0; d < ALL_COUNT; ++d)
			{
				divide_windows[d].Dispose();
				divide_windows[d] = null;
			}
			divide_windows = null;
		}

	}
}
