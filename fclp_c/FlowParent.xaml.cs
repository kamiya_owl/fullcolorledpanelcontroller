﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DirectShowLib;
using System.Threading;
using System.Runtime.InteropServices;
using OpenCvSharp;
using System.Drawing;
using System.IO.Ports;
using System.Collections;
using System.Windows.Threading;
using System.IO;
using System.Threading.Tasks;

namespace fclp_c
{
	/// <summary>
	/// MainWindow.xaml の相互作用ロジック
	/// </summary>
	public partial class FlowParent : Window,IDisposable
	{
		//serialport
		private SerialPort _serialport = new SerialPort();
		private bool _serialportNeedWarning = false;//切断された時の通知用、trueの時開いている
		private CvWindow _sendPreviewWindow = null;
		private CvWindow[] _divideWindows = null;
		private bool _isShowSendPreview = false;
		//drawing
		Window window = null;//(IFPGASendableに変換可能なもの)
		//option
		ProgramSettingWindow _psWindow = null;
		//Notify
		private DispatcherTimer _notifyObserberThread = null;
		private int _notifyInterval = 5;//s

		public FlowParent()
		{
			InitializeComponent();
			//Initailize
			AutoNotifyCheckStart();

		}
		/////////////////////////////////////////////////////////////////////////////////
		//FPGASender

		/// <summary>
		/// 引数に指定された画像を変換して送信する。
		/// FPGAFormat.SendFromIplImageDelegateに指定することができる。
		/// </summary>
		/// <param name="origin_image">送信する画像</param>
		/// <returns>変換して送信できたかを示す真偽値</returns>
		public bool SendFromIplImage(IplImage origin_image,bool needFlip)
		{
            if (origin_image == null || origin_image.IsDisposed == true) return false;
			//クローンして他と干渉しないようにする
			IplImage send_image = null;
			//上下反転しないようにする
			if (needFlip)
			{
				send_image = new IplImage(origin_image.GetSize(), origin_image.Depth, origin_image.NChannels);
				Cv.Flip(origin_image, send_image, FlipMode.X);
			}
			else
			{
				//クローン
				send_image = Cv.CloneImage(origin_image);
			}

			//プレビュー
			if (_isShowSendPreview)
			{
				IplImage resize_image = new IplImage(FPGASender.ALL_WIDTH, FPGASender.ALL_HEIGHT, BitDepth.U8, 3);
				//送信イメージを設定されたサイズでリサイズ
				resize_image = FPGASender.PreviewResizeImage(send_image);
				//表示
				if (_sendPreviewWindow != null) _sendPreviewWindow.ShowImage(resize_image);
				else _isShowSendPreview = false;

				//分割プレビュー
				IplImage[] divide_images = FPGASender.PreviewDivideImage(send_image);
				FPGASender.PreviewDivideWindowShowImage(divide_images, _divideWindows);


				//開放
				FPGASender.PreviewDivideImageDispose(divide_images);
				resize_image.Dispose();
				resize_image = null;
			}


			//送信するかチェック
			if (!_serialportNeedWarning || !(bool)XIsEnabledCheckBox.IsChecked) return false;//!_serialportNeedWarningは!_serialport.IsOpenの代わり
			if (!_serialport.IsOpen)
			{
				_serialportNeedWarning = false;
				NotifyMessage(this, "シリアルポート\'{0}\'は切断されました。\nデータは送信されません。", _serialport.PortName);
				return false;
			}
			//みため
			SerialPortIndicatorIncrement();

			//ファイル出力
			if (XExportCSVDataCheckBox.IsChecked == true)
			{
				byte[] buffer = new byte[FPGASender.ALL_BUFFER_SIZE];
				FPGASender.ConvertFPGAFormat(send_image, ref buffer);
				try
				{
					using (StreamWriter sw = new StreamWriter("data.csv", false, Encoding.Default))
					{
						for (int p = 0; p < FPGASender.ALL_COUNT; ++p)
						{
							for (int d = 0; d < FPGASender.PANEL_BUFFER_SIZE; ++d)
							{
								sw.Write("{0:d3},", buffer[d + p * FPGASender.PANEL_BUFFER_SIZE]);
							}
							sw.WriteLine();
						}
					}
				}
				catch (Exception e)
				{
					NotifyMessage(e, "csv出力を終了します。");
					XExportCSVDataCheckBox.IsChecked = false;
					return false;
				}
				//buffer = null;
			}
			//送信TODO:送信周りをもっと綺麗にまとめる
			bool returnValue = false;
			if (XUseAsyncSendCheckBox.IsChecked == true)
			{
				if (!_isSending)
				{
					Task.Factory.StartNew<bool>(() =>
					{
						try
						{
							//排他ロック
							_isSending = true;
							returnValue = FPGASender.Send(_serialport, send_image);
						}
						catch (Exception e)
						{
							NotifyMessage(e, "データの送信に失敗(非同期送信)");
						}
						finally
						{
							send_image.Dispose();
							send_image = null;
							_isSending = false;
						}
						return returnValue;
					});
				}
				else
				{
					//送信は行わない
					//Console.Write(".");
				}
			}
			else
			{
				try
				{
					returnValue = FPGASender.Send(_serialport, send_image);
				}
				catch (Exception e)
				{
					NotifyMessage(e, "データの送信に失敗");
				}
				finally
				{
					send_image.Dispose();
					send_image = null;
				}
			}
			return returnValue;
		}
		volatile bool _isSending = false;
		private bool SerialOpen()
		{
			//シリアルポートを開く
			if (_serialport.IsOpen) return true;
			if (string.IsNullOrWhiteSpace(XPortNameTextBox.Text)) return false;
			try
			{
				//シリアルポート設定
				_serialport.BaudRate = FPGASender.BaudRate;
				_serialport.Parity = FPGASender.ParityBit;
				_serialport.DataBits = FPGASender.DataBit;
				_serialport.StopBits = FPGASender.StopBit;
				_serialport.PortName = XPortNameTextBox.Text;

				_serialport.Open();
				//通知とか
				NotifyMessage(this, "シリアルポート\'{0}\'の接続を確立しました。", _serialport.PortName);
				_serialportNeedWarning = true;
				//UpdateSerialPortUI(true);
				return true;
			}
			catch (Exception e)
			{
				NotifyMessage(e,"シリアルポートの接続に失敗");
				return false;
			}
		}
		private bool SerialClose()
		{
			//if (!_serialport.IsOpen) return true;
			try
			{
				_serialport.Close();
				//
				NotifyMessage(this, "シリアルポート\'{0}\'を切断しました。", _serialport.PortName);
				//UpdateSerialPortUI(false);
				return true;
			}
			catch (Exception e)
			{
				NotifyMessage(e, "シリアルポートの切断に失敗");
				return false;
			}
		}
		/////////////////////////////////////////////////////////////////////////////////
		//Notify
		//delegate
		public bool NotifyMessage(object sender, string message, params object[] args)
		{
            Dispatcher.BeginInvoke(new Action(() =>
                {
                    NotifyControl notify = new NotifyControl(sender.ToString(), string.Format(message, args));
                    notify.XCloseButton.MouseDown += (dsender, de) =>
                    {
                        //notify.IsEnabledAnimationBind = false;
                        XNotifyPanel.Children.Remove(notify);
                    };
                    //追加
                    XNotifyPanel.Children.Add(notify);
                }));
			return true;
		}
		public bool NotifyMessage(Exception e ,string message)
		{
			return NotifyMessage(e.Source, message + "\n{0}",e.Message);
		}
		private void AutoNotifyCheckStart()
		{
			if (_notifyObserberThread != null)
			{
				_notifyObserberThread.Stop();
				_notifyObserberThread = null;
				
			}
			_notifyObserberThread = new DispatcherTimer();
			_notifyObserberThread.Interval = new TimeSpan(0, 0, 0, _notifyInterval, 0);
			_notifyObserberThread.Tick += new EventHandler(_notifyObserberThread_Tick);
			_notifyObserberThread.Start();
		}
		private void AutoNotifyCheckStop()
		{
			if (_notifyObserberThread.IsEnabled)
			{
				_notifyObserberThread.Stop();
			}
		}
		void _notifyObserberThread_Tick(object sender, EventArgs e)
		{
			//TODO:使用しない場合の処理
			if (XNotifyPanel.Children.Count != 0)
			{
				//削除
				XNotifyPanel.Children.RemoveAt(0);
			}
		}
		/////////////////////////////////////////////////////////////////////////////////
		//Title Event
		private void XEndButton_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			if (window != null)
				if (window.IsVisible == true)
					window.Close();	
			this.Close();
		}

		/////////////////////////////////////////////////////////////////////////////////
		//Serial Event
		private void SerialPortIndicatorIncrement()
		{
			//一個足す
			++XSendIndicator.Value;
			if (XSendIndicator.Value == XSendIndicator.Maximum) XSendIndicator.Value = XSendIndicator.Minimum;
		}

		private void UpdateSerialPortUI(bool isEnabled)
		{
			XOpenButton.IsEnabled = !isEnabled;
			XCloseButton.IsEnabled = isEnabled;
		}

		private void XOpenButton_Click(object sender, RoutedEventArgs e)
		{
			SerialOpen();
		}

		private void XCloseButton_Click(object sender, RoutedEventArgs e)
		{
			SerialClose();
		}
		void IDisposable.Dispose()
		{
			//開放
			SerialClose();
			_serialport.Dispose();
			_serialport = null;
		}
		private void XShowSendImageButton_Click(object sender, RoutedEventArgs e)
		{
			//プレビューを作成
			_isShowSendPreview = true;
			_sendPreviewWindow = new CvWindow("Send Preview", WindowMode.FreeRatio);
			_divideWindows = FPGASender.PreviewDivideWindowInitialize();

			XShowSendImageButton.IsEnabled = false;
			XCloseSendImageButton.IsEnabled = true;

			NotifyMessage(this, "送信プレビューを表示しました。\nFPGAの送信の変換とは別に変換を行なっているので、デバッグ程度の使用に抑えてください。");
		}

		private void XCloseSendImageButton_Click(object sender, RoutedEventArgs e)
		{
			_isShowSendPreview = false;
			_sendPreviewWindow.Dispose();
			_sendPreviewWindow = null;
			FPGASender.PreviewDivideWindowFinalize(_divideWindows);

			XShowSendImageButton.IsEnabled = true;
			XCloseSendImageButton.IsEnabled = false;

			NotifyMessage(this, "送信プレビューを閉じました。");
		}

		private void XExportCSVDataCheckBox_Checked(object sender, RoutedEventArgs e)
		{
			NotifyMessage(this, "送信するデータをcsv形式で出力します。");
		}
		private void XUseAsyncSendCheckBox_Checked(object sender, RoutedEventArgs e)
		{
			NotifyMessage(this, "非同期送信を使用します。");
		}

		/////////////////////////////////////////////////////////////////////////////////
		//Drawing Event
		private void XOpenWindowButton_Click(object sender, RoutedEventArgs e)
		{
			//新しい描画ツールを作ったらここで追加
			if (XDirectShowRadioButton.IsChecked == true) window = new DirectShowWindow();
			else if (XShowTestRadioButton.IsChecked == true) window = new TestPatternWindow();
			else if (XShowImageRadioButton.IsChecked == true) window = new ShowImageWindow();
            else if (XShowTextRadioButton.IsChecked == true) window = new ShowTextWindow();
            else if (XAnimationRadioButton.IsChecked == true) window = new AnimationWindow();
            else if (XMicInputRadioButton.IsChecked == true) window = new MicInputWindow();
            else if (XTetrisRadioButton.IsChecked == true) window = new TetrisWindow();
            //interface
			IFPGASendable sendable = (IFPGASendable)window;
			sendable.SendFromIplImage += new FPGASender.SendFromIplImageDelegate(SendFromIplImage);
			sendable.NotifyMessage += new FPGASender.NotifyMessageDelegate(NotifyMessage);
			//window event
			window.Loaded += new RoutedEventHandler(OpenChildrenWindow);
			window.Closed += new EventHandler(CloseChildrenWindow);
			window.Show();
		}
		//Delegate
		public void OpenChildrenWindow(object sender, EventArgs args)
		{
			XDrawingList.IsEnabled = false;
			XOpenWindowButton.IsEnabled = false;
			((IFPGASendable)sender).Initialize();//初期化処理を読んでおく
			//
			NotifyMessage(sender,"{0}を表示しました。",window.Title);
		}

		public void CloseChildrenWindow(object sender, EventArgs args)
		{
			XDrawingList.IsEnabled = true;
			XOpenWindowButton.IsEnabled = true;
			((IFPGASendable)sender).Finalize();//終了処理を読んでおく

			NotifyMessage(sender, "{0}を閉じました。", window.Title);
		}

		/////////////////////////////////////////////////////////////////////////////////
		//Option Event
		private void XProgramOptionButton_Click(object sender, RoutedEventArgs e)
		{
			if (_psWindow != null) return;
			if (MessageBox.Show("設定を変更するには現在開いているプレビュー、描画ウインドウを閉じる必要があります。続行しますか？","警告",MessageBoxButton.YesNo) == MessageBoxResult.Yes)
			{
				//プレビューを消す
				if (XCloseSendImageButton.IsEnabled == true) XCloseSendImageButton_Click(this, null);
				if (XOpenWindowButton.IsEnabled == false) window.Close();
				_psWindow = new ProgramSettingWindow();
				_psWindow.Closed += (csender, ce) =>
				{
					_psWindow = null;
					NotifyMessage(csender, "設定ウインドウを閉じました。");
				};
				_psWindow.NotifyMessage += new FPGASender.NotifyMessageDelegate(NotifyMessage);
				_psWindow.Show();

				NotifyMessage(this, "設定ウインドウを表示しました。\n設定内容は試験段階なので注意して変更してください。");
			}
		}

	}
}
