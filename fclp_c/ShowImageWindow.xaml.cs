﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using OpenCvSharp;

namespace fclp_c
{
	/// <summary>
	/// ShowImageWindow.xaml の相互作用ロジック
	/// </summary>
	public partial class ShowImageWindow : Window,IFPGASendable
	{
		//thread
		public FPGASender.SendFromIplImageDelegate SendFromIplImage { get; set; }
		public FPGASender.NotifyMessageDelegate NotifyMessage { get; set; }
		int _interval = FPGASender.MASTER_INTERVAL;//Tick[ms]
		DispatcherTimer _updateThread = null;
		//image
		IplImage _sendImage = null;
		CvWindow _previewWindow = null;
		IplImage _cropBaseImage = null;
		CvWindow _cropBasePreviewWindow = null;

		public ShowImageWindow()
		{
			InitializeComponent();
		}
		//////////////////////////////////////////////////////////////////////////////////////////////
		//Delegate
		bool IFPGASendable.Initialize()
		{
			_previewWindow = new CvWindow("Load Image", WindowMode.FreeRatio);
			InitializeThread();
			return true;
		}

		void IFPGASendable.Stop()
		{
			FinalizeThread();
		}

		bool IFPGASendable.Finalize()
		{
			FinalizeThread();
			if (_sendImage != null)
			{
				_sendImage.Dispose();
				_sendImage = null;
			}
			if (_previewWindow != null)
			{
				_previewWindow.Dispose();
				_previewWindow = null;
			}
			if (_cropBaseImage != null)
			{
				_cropBaseImage.Dispose();
				_cropBaseImage = null;
			}
			if (_cropBasePreviewWindow != null)
			{
				_cropBasePreviewWindow.Dispose();
				_cropBasePreviewWindow = null;
			}
			return true;
		}
		//////////////////////////////////////////////////////////////////////////////////////////////
		//Thread
		private void InitializeThread()
		{
			FinalizeThread();

			_updateThread = new DispatcherTimer();
			_updateThread.Interval = new TimeSpan(0, 0, 0, 0, _interval);
			_updateThread.Tick +=new EventHandler(_updateThread_Tick);
			_updateThread.Start();


		}
		private void ChangeThreadInterval()
		{
			if (_updateThread != null)
			{
				_updateThread.Stop();
				_updateThread.Interval = new TimeSpan(0, 0, 0, 0, _interval);
				_updateThread.Start();
			}

		}
		private void FinalizeThread()
		{
			if (_updateThread != null)
			{
				if (_updateThread.IsEnabled) _updateThread.Stop();
				_updateThread = null;
			}
		}
		//DispatcherTimer
		void _updateThread_Tick(object sender, EventArgs e)
		{
			if (_sendImage == null) return;

			//AutoScroll
			if (XAutoScrollXCheckBox.IsChecked == true)
			{
				++XCropXSlider.Value;
				if (XCropXSlider.Value == XCropXSlider.Maximum)
				{
					XCropXSlider.Value = 0;
				}
			}
			if (XAutoScrollYCheckBox.IsChecked == true)
			{
				++XCropYSlider.Value;
				if (XCropYSlider.Value == XCropYSlider.Maximum)
				{
					XCropYSlider.Value = 0;
				}
			}

			//コピー
			IplImage copy_image = Cv.CloneImage(_sendImage);
			//送信
			SendFromIplImage(copy_image,false);
			//プレビュー
			if (_previewWindow != null) _previewWindow.ShowImage(copy_image);

			//開放
			copy_image.Dispose();
			copy_image = null;
		}


		private void CropImage()
		{
			if (_sendImage != null)
			{
				_sendImage.Dispose();
				_sendImage = null;
			}
			//ROIで切り出し
			Cv.SetImageROI(_cropBaseImage, new CvRect((int)XCropXSlider.Value, (int)XCropYSlider.Value, FPGASender.ALL_WIDTH, FPGASender.ALL_HEIGHT));
			_sendImage = Cv.CloneImage(_cropBaseImage);
			Cv.ResetImageROI(_cropBaseImage);
		}
		//////////////////////////////////////////////////////////////////////////////////////////////
		//Event
		private void XDragDropArea_PreviewDragOver(object sender, DragEventArgs e)
		{
			//FileDropを許可
			e.Handled = true;// (e.Data.GetData(DataFormats.FileDrop) != null);
		}

		private void XDragDropArea_Drop(object sender, DragEventArgs e)
		{
			//cropは無効化する
			XUseCropCheckBox.IsChecked = false;
			//ドロップされた画像ファイルを出力
			string[] filename = (string[])e.Data.GetData(DataFormats.FileDrop);
			if (filename.Length > 1) NotifyMessage(this, "複数ファイルのドロップは行うことができません。{0}を読み込みます。", filename[0]);

			if (_sendImage != null)
			{
				_sendImage.Dispose();
				_sendImage = null;
			}
			try
			{
				_sendImage = new IplImage(filename[0], LoadMode.AnyColor);
				//読み込み失敗
			}
			catch (Exception ex)
			{
				NotifyMessage(ex.Source, "{0}の読み込みに失敗しました。\n{1}\n{2}", filename[0],ex.Message,ex.StackTrace);
				_sendImage = null;
				return;
			}

			NotifyMessage(this, "{0}の読み込みが完了しました。", filename[0]);
			
		}
		private void XUseCropCheckBox_Checked(object sender, RoutedEventArgs e)
		{
			if(_sendImage == null)
			{
				NotifyMessage(this,"画像が指定されていないので使用できません。");
				return;
			}
			int cropAreaWidth = _sendImage.Width - FPGASender.ALL_WIDTH;
			int cropAreaHeight = _sendImage.Height - FPGASender.ALL_HEIGHT;
			if ((cropAreaWidth < 0) || (cropAreaHeight < 0))
			{
				NotifyMessage(this, "元画像の大きさがLEDパネルの解像度より小さいので切り取りを行うことはできません。");
				return;
			}

			//ベースイメージの決定
			if (_cropBaseImage != null)
			{
				_cropBaseImage.Dispose();
				_cropBaseImage = null;
			}
			_cropBaseImage = Cv.CloneImage(_sendImage);
			//プレビュー
			if (_cropBasePreviewWindow == null)
			{
				_cropBasePreviewWindow = new CvWindow("Crop Base Image", WindowMode.FreeRatio);
			}
			_cropBasePreviewWindow.ShowImage(_cropBaseImage);
			//入れ替え
			XCropXSlider.Value = 0;
			XCropYSlider.Value = 0;
			XCropXSlider.Maximum = _sendImage.Width - FPGASender.ALL_WIDTH;
			XCropYSlider.Maximum = _sendImage.Height - FPGASender.ALL_HEIGHT;
			XCropXSlider.LargeChange = FPGASender.PANEL_WIDTH;
			XCropYSlider.LargeChange = FPGASender.PANEL_HEIGHT;
			XCropXSlider.SmallChange = 1;
			XCropYSlider.SmallChange = 1;
			XAutoScrollXCheckBox.IsChecked = false;
			XAutoScrollYCheckBox.IsChecked = false;

			CropImage();
		}

		private void XUseCropCheckBox_Unchecked(object sender, RoutedEventArgs e)
		{
			if ((_sendImage == null)||(_cropBaseImage == null)) return;
			//cropbase -> send
			_sendImage.Dispose();
			_sendImage = _cropBaseImage;
			_cropBaseImage = null;
			//
			if (_cropBasePreviewWindow != null)
			{
				_cropBasePreviewWindow.Dispose();
				_cropBasePreviewWindow = null;
			}
		}

		private void UpdateCropRect(object sender, RoutedPropertyChangedEventArgs<double> e)
		{
			if (XUseCropCheckBox.IsChecked == false) return;
			CropImage();
		}
	}
}
