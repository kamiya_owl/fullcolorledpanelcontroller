﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Drawing;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Threading;
using OpenCvSharp;
using Microsoft.DirectX;
using Microsoft.DirectX.DirectSound;

namespace fclp_c
{
    /// <summary>
    /// MicInputWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MicInputWindow : Window, IFPGASendable
    {

        //サンプリング周波数44.1kHzで0.1秒 バッファ要素数44100
        const int BUFFER_SIZE = 4096;
        const double HEIGHT_ADJUSTMENT = 0.7;
        const double NOISE_ADJUSTMENT = 1.1;
        double height_adustment = HEIGHT_ADJUSTMENT;
        double noise_adustment = NOISE_ADJUSTMENT;
        WaveFormat waveFormat = new WaveFormat();
        CaptureBufferDescription captureBufferDescription = new CaptureBufferDescription();
        Device device = new Device();
        CaptureDevicesCollection captureDevices = null;
        Capture capture;
        CaptureBuffer captureBuffer;
        System.Drawing.Point[] pt = new System.Drawing.Point[BUFFER_SIZE];
        //ImageオブジェクトのGraphicsオブジェクトを作成する
        Graphics g;
        System.Drawing.Rectangle[] rect = new System.Drawing.Rectangle[BUFFER_SIZE];
        Int16[] CaptureData = null;
        const int DRAWWIDTH = 64;
        double[] Data = new double[BUFFER_SIZE];
        double[] Draw;
        double[] Noise;

        //delegate
        public FPGASender.SendFromIplImageDelegate SendFromIplImage { get; set; }
        public FPGASender.NotifyMessageDelegate NotifyMessage { get; set; }

        public Thread drawingThread = null;
        private CvWindow previewWindow = new CvWindow("MicInputPreview", WindowMode.FreeRatio);
        private volatile int fps = 24;

        private enum Mode { Spectrum, Wave };
        private Mode mode = Mode.Spectrum;


        public MicInputWindow()
        {
            InitializeComponent();
            //UI Initialize here...

        }


        public bool Initialize()
        {
            //Initialize here
            drawingThread = new Thread(new ThreadStart(OnDraw));
            drawingThread.Start();

            UpdateSoundDeviceList();
            InitializeCaptureDevice();

            return true;
        }
        private void UpdateSoundDeviceList()
        {
            int selected = XSoundDeviceList.SelectedIndex;
            XSoundDeviceList.Items.Clear();
            XSoundDeviceList.SelectedIndex = -1;

            captureDevices = new CaptureDevicesCollection();
            foreach(var cd in captureDevices)
            {
                XSoundDeviceList.Items.Add(cd);
            }
            if(selected < XSoundDeviceList.Items.Count)
            {
                XSoundDeviceList.SelectedIndex = selected;
            }
            if (selected == -1 && XSoundDeviceList.Items.Count != 0)
            {
                XSoundDeviceList.SelectedIndex = 0;
            }

        }
        private void InitializeCaptureDevice()
        {
            Draw = new double[DRAWWIDTH];
            Noise = new double[DRAWWIDTH];
            height_adustment = HEIGHT_ADJUSTMENT;
            noise_adustment = NOISE_ADJUSTMENT;

            XTextHeight.Text = height_adustment.ToString();
            XTextNoise.Text = noise_adustment.ToString();

            // waveのせってい
            waveFormat.Channels = 1;                     //モノラル
            waveFormat.FormatTag = WaveFormatTag.Pcm;    //PCM指定
            waveFormat.BitsPerSample = 16;               //16bit
            waveFormat.SamplesPerSecond = 44100;         //44.1KHz
            waveFormat.BlockAlign = (short)(waveFormat.Channels * (waveFormat.BitsPerSample / (short)8));//1サンプルあたりのバイト数
            waveFormat.AverageBytesPerSecond = waveFormat.BlockAlign * waveFormat.SamplesPerSecond;//1秒間あたりのバイト数

            // バッファの確保
            //バッファを0.1秒分確保
            captureBufferDescription.BufferBytes = waveFormat.AverageBytesPerSecond / 10;
            captureBufferDescription.Format = waveFormat;

            // 録音デバイスの準備
            DeviceInformation deviceInfo = captureDevices[XSoundDeviceList.SelectedIndex];
            if (capture != null)
            {
                capture.Dispose();
                capture = null;
            }
            capture = new Capture(deviceInfo.DriverGuid);

            //バッファ作成
            try
            {
                //フォーマット構造体・デバイス指定 buffer取得
                if (captureBuffer != null)
                {
                    captureBuffer.Dispose();
                    captureBuffer = null;
                }
                captureBuffer = new CaptureBuffer(captureBufferDescription, capture);
            }
            catch
            {
                NotifyMessage(this, "録音デバイスが無いか、録音フォーマットをサポートしていません。");
                //Close();
            }
            if (null == capture || captureBuffer == null)
            {
                throw new NullReferenceException();
            }
            captureBuffer.Start(true);
        }
        public void Stop()
        {
            Finalize();
        }

        public bool Finalize()
        {
            if (drawingThread != null && drawingThread.IsAlive)
            {
                drawingThread.Abort();
                drawingThread = null;
            }
            if (previewWindow != null)
            {
                previewWindow.Dispose();
                previewWindow = null;
            }
            captureBuffer.Stop();
            //リソースを解放する
            capture.Dispose();
            captureBuffer.Dispose();
            return true;
        }

        // byteから論理シフトのビット(?)への関数
        public static int LogicalShiftBit(int b)
        {
            int bit = 0;
            int i;
            for (i = 2; i <= b; i *= 2)
            {
                bit++;
            }
            return bit;
        }

        // FFT
        public static void FFT(double[] inputRe, double[] inputIm, out double[] outputRe, out double[] outputIm, int bitSize)
        {
            int dataSize = 1 << bitSize;
            int[] reverseBitArray = BitScrollArray(dataSize);

            outputRe = new double[dataSize];
            outputIm = new double[dataSize];

            // バタフライ演算のための置き換え
            for (int i = 0; i < dataSize; i++)
            {
                outputRe[i] = inputRe[reverseBitArray[i]];
                outputIm[i] = inputIm[reverseBitArray[i]];
            }

            // バタフライ演算
            for (int stage = 1; stage <= bitSize; stage++)
            {
                int butterflyDistance = 1 << stage;
                int numType = butterflyDistance >> 1;
                int butterflySize = butterflyDistance >> 1;

                double wRe = 1.0;
                double wIm = 0.0;
                double uRe = System.Math.Cos(System.Math.PI / butterflySize);
                double uIm = -System.Math.Sin(System.Math.PI / butterflySize);

                for (int type = 0; type < numType; type++)
                {
                    for (int j = type; j < dataSize; j += butterflyDistance)
                    {
                        int jp = j + butterflySize;
                        double tempRe = outputRe[jp] * wRe - outputIm[jp] * wIm;
                        double tempIm = outputRe[jp] * wIm + outputIm[jp] * wRe;
                        outputRe[jp] = outputRe[j] - tempRe;
                        outputIm[jp] = outputIm[j] - tempIm;
                        outputRe[j] += tempRe;
                        outputIm[j] += tempIm;
                    }
                    double tempWRe = wRe * uRe - wIm * uIm;
                    double tempWIm = wRe * uIm + wIm * uRe;
                    wRe = tempWRe;
                    wIm = tempWIm;
                }
            }
        }

        // ビットを左右反転した配列を返す
        private static int[] BitScrollArray(int arraySize)
        {
            int[] reBitArray = new int[arraySize];
            int arraySizeHarf = arraySize >> 1;

            reBitArray[0] = 0;
            for (int i = 1; i < arraySize; i <<= 1)
            {
                for (int j = 0; j < i; j++)
                    reBitArray[j + i] = reBitArray[j] + arraySizeHarf;
                arraySizeHarf >>= 1;
            }
            return reBitArray;
        }

        private void OnDraw()
        {
            // ここが毎回呼ばれる
            while (true)
            {
                using (var drawing = new IplImage(FPGASender.ALL_WIDTH, FPGASender.ALL_HEIGHT, BitDepth.U8, 3))
                {
                    //draw
                    Cv.Set(drawing, new CvColor(0, 0, 0));

                    if (captureBuffer != null)
                    {

                        CaptureData = (Int16[])captureBuffer.Read(0, typeof(Int16), LockFlag.None, BUFFER_SIZE);
                        //mode
                        switch (mode)
                        {
                            case Mode.Spectrum:
                                DrawSpectrum(drawing);
                                break;
                            case Mode.Wave:
                                DrawWave(drawing);
                                break;
                            default:
                                break;
                        }
                        //send
                        Dispatcher.BeginInvoke(new Action(() =>
                        {
                            using (var send = new IplImage(FPGASender.ALL_WIDTH, FPGASender.ALL_HEIGHT, BitDepth.U8, 3))
                            {
                                SendFromIplImage(drawing, false);
                            }
                        }));
                        //preview
                        previewWindow.ShowImage(drawing);
                        //wait
                        Thread.Sleep(1000 / fps);

                    }

                }
            }
        }

        private void DrawWave(IplImage drawing)
        {
            int t;
            int i, j;
            int width;
            //要素数
            t = LogicalShiftBit(BUFFER_SIZE) - LogicalShiftBit(DRAWWIDTH);
            width = FPGASender.ALL_WIDTH / DRAWWIDTH;

            for (i = 0; i < BUFFER_SIZE; i++)
            {
                Data[i] = (double)CaptureData[i] / 32767;
            }


            for (i = 0; i < DRAWWIDTH; i++)
            {
                Draw[i] = 0;
                pt[i].X = i;
                for (j = 0; j < t; j++)
                {
                    Draw[i] += Data[i * t + j] / t;
                }
                pt[i].Y = (int)(FPGASender.ALL_HEIGHT - (Draw[i]) * height_adustment * FPGASender.ALL_HEIGHT);

                rect[i].X = i * width;
                rect[i].Width = width;
                rect[i].Y = pt[i].Y;
                rect[i].Height = FPGASender.ALL_HEIGHT - pt[i].Y;
                //if (rect[i].Y < 0) rect[i].Y = 0;
                Cv.DrawRect(drawing, rect[i].X, rect[i].Y, rect[i].X + rect[i].Width, FPGASender.ALL_HEIGHT, new CvScalar(0, 0xff, 0));
            }


        }
        private void DrawSpectrum(IplImage drawing)
        {
            int t;
            int i, j;
            int width;
            // FFT用データ
            double[] reFFT;
            double[] imFFT;
            double[] fftInIm = new double[BUFFER_SIZE];
            //要素数
            for (i = 0; i < BUFFER_SIZE; i++)
            {
                Data[i] = (double)CaptureData[i];
                fftInIm[i] = 0.0;
            }


            FFT(Data, fftInIm, out reFFT, out imFFT, LogicalShiftBit(BUFFER_SIZE));
            t = LogicalShiftBit(BUFFER_SIZE) - LogicalShiftBit(DRAWWIDTH);
            width = FPGASender.ALL_WIDTH / DRAWWIDTH;

            for (i = 0; i < BUFFER_SIZE; i++)
            {
                Data[i] = Math.Log10(Math.Sqrt(reFFT[i] * reFFT[i] + imFFT[i] * imFFT[i]));
            }


            for (i = 0; i < DRAWWIDTH; i++)
            {
                Draw[i] = 0;
                pt[i].X = i;
                for (j = 0; j < t; j++)
                {
                    Draw[i] += Data[i * t + j] / t;
                }
                pt[i].Y = (int)(FPGASender.ALL_HEIGHT - (Draw[i] - Noise[i]) * height_adustment * FPGASender.ALL_HEIGHT);

                rect[i].X = i * width;
                rect[i].Width = width;
                rect[i].Y = pt[i].Y;
                rect[i].Height = FPGASender.ALL_HEIGHT - pt[i].Y;
                //if (rect[i].Y < 0) rect[i].Y = 0;
                Cv.DrawRect(drawing, rect[i].X, rect[i].Y, rect[i].X + rect[i].Width, FPGASender.ALL_HEIGHT, new CvColor((int)(0xff * (i / (double)DRAWWIDTH)), (int)(0xff * (1.0 - (i / (double)DRAWWIDTH) / 2)), (int)(0xff * (1.0 - (i / (double)DRAWWIDTH)))));
            }

        }

        private void XResetEnviroNoise_Click(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i < DRAWWIDTH; i++)
            {
                Noise[i] = Draw[i] * noise_adustment;
            }

        }

        private void XUpdateList_Click(object sender, RoutedEventArgs e)
        {
            UpdateSoundDeviceList();
        }

        private void XChangeDevice_Click(object sender, RoutedEventArgs e)
        {
            captureBuffer.Stop();
            InitializeCaptureDevice();
        }

        private void XUseSpectrum_Checked(object sender, RoutedEventArgs e)
        {
            mode = Mode.Spectrum;
        }

        private void XUseWave_Checked(object sender, RoutedEventArgs e)
        {
            mode = Mode.Wave;
        }

        private void XSetHeight_Click(object sender, RoutedEventArgs e)
        {
            height_adustment = double.Parse(XTextHeight.Text);
        }

        private void XSetNoise_Click(object sender, RoutedEventArgs e)
        {
            double old_noise; // ノイズ倍率一時保存
            old_noise = noise_adustment;
            noise_adustment = double.Parse(XTextNoise.Text);
            for (int i = 0; i < DRAWWIDTH; i++)
            {
                Noise[i] = Draw[i] / old_noise * noise_adustment;
            }
        }
    }
}
