﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Threading;
using OpenCvSharp;
using System.Diagnostics;

namespace fclp_c
{
    /// <summary>
    /// TetrisWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class TetrisWindow : Window, IFPGASendable
    {
        private volatile int fps = 24;
        private Thread drawingThread = null;
        private CvWindow previewWindow = new CvWindow("AnimationPreview", WindowMode.FreeRatio);


        public FPGASender.SendFromIplImageDelegate SendFromIplImage { get; set; }
        public FPGASender.NotifyMessageDelegate NotifyMessage { get; set; }


        public TetrisWindow()
        {
            InitializeComponent();
        }

        public bool Initialize()
        {
            drawingThread = new Thread(new ThreadStart(OnDraw));
            drawingThread.Start();

            return true;
        }
        public void Stop()
        {
            Finalize();
        }

        public bool Finalize()
        {
            if (drawingThread != null && drawingThread.IsAlive)
            {
                drawingThread.Abort();
                drawingThread = null;
            }
            if (previewWindow != null)
            {
                previewWindow.Dispose();
                previewWindow = null;
            }

            return true;
        }
        private void OnDraw()
        {

            var sw = new Stopwatch();
            //loop
            while (true)
            {
                using (var drawing = new IplImage(FPGASender.ALL_WIDTH, FPGASender.ALL_HEIGHT, BitDepth.U8, 3))
                {
                    //timer
                    sw.Restart();
                    //draw
                    Cv.Set(drawing,new CvScalar(0xff,0,0xff));

                    //send
                    Dispatcher.BeginInvoke(new Action(() =>
                    {
                        using (var send = new IplImage(FPGASender.ALL_WIDTH, FPGASender.ALL_HEIGHT, BitDepth.U8, 3))
                        {

                            SendFromIplImage(drawing, false);
                        }
                    }));
                    //preview
                    previewWindow.ShowImage(drawing);
                    //wait
                    Thread.Sleep(Math.Abs(1000 / fps - (int)sw.ElapsedMilliseconds));
                }
            }

        }


    }
}
