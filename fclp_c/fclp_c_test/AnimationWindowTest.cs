﻿using fclp_c;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using OpenCvSharp;
using System.Windows;

namespace fclp_c_test
{
    
    
    /// <summary>
    ///AnimationWindowTest のテスト クラスです。すべての
    ///AnimationWindowTest 単体テストをここに含めます
    ///</summary>
    [TestClass()]
    public class AnimationWindowTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///現在のテストの実行についての情報および機能を
        ///提供するテスト コンテキストを取得または設定します。
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region 追加のテスト属性
        // 
        //テストを作成するときに、次の追加属性を使用することができます:
        //
        //クラスの最初のテストを実行する前にコードを実行するには、ClassInitialize を使用
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //クラスのすべてのテストを実行した後にコードを実行するには、ClassCleanup を使用
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //各テストを実行する前にコードを実行するには、TestInitialize を使用
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //各テストを実行した後にコードを実行するには、TestCleanup を使用
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///NoneDraw のテスト
        ///</summary>
        [TestMethod()]
        [DeploymentItem("fclp_c.exe")]
        public void NoneDrawTest()
        {
            var window = new AnimationWindow();
            //interface
            IFPGASendable sendable = (IFPGASendable)window;
            sendable.SendFromIplImage += new FPGASender.SendFromIplImageDelegate(SendFromIplImage);
            sendable.NotifyMessage += new FPGASender.NotifyMessageDelegate(NotifyMessage);
            window.Initialize();
            window.ShowDialog();
            //AnimationWindow_Accessor target = new AnimationWindow_Accessor(); // TODO: 適切な値に初期化してください
            //IplImage drawing = null; // TODO: 適切な値に初期化してください
            //target.NoneDraw(drawing);
            //Assert.Inconclusive("値を返さないメソッドは確認できません。");
        }
        public bool SendFromIplImage(IplImage origin_image, bool needFlip)
        {
            CvWindow.ShowImages(origin_image);
            return true;
        }
        public bool NotifyMessage(object sender, string message, params object[] args)
        {
            MessageBox.Show(string.Format(message, args));
            return true;
        }

    }
}
