﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace fclp_c
{
	/// <summary>
	/// ProgramSettingWindow.xaml の相互作用ロジック
	/// </summary>
	public partial class ProgramSettingWindow : Window,IFPGASendable
	{
		//delegate
		public FPGASender.NotifyMessageDelegate NotifyMessage { get; set; }

		public ProgramSettingWindow()
		{
			InitializeComponent();
			//表示を更新
			LoadValue();
		}

		private void SetNewValue()
		{
			try
			{
				//定数値
				FPGASender.PANEL_WIDTH = int.Parse(XPANELWIDTHText.Text);
				FPGASender.PANEL_HEIGHT = int.Parse(XPANELHEIGHTText.Text);
				FPGASender.PANEL_CHANNELS = int.Parse(XPANELCHANNELSText.Text);
				FPGASender.ALL_COLUMN = int.Parse(XALLCOLUMNText.Text);
				FPGASender.ALL_ROW = int.Parse(XALLROWText.Text);
				FPGASender.MASTER_INTERVAL = int.Parse(XMASTERINTERVALText.Text);
				//計算値
				if (XUnlockCalculationSetting.IsChecked == true)
				{
					FPGASender.ALL_WIDTH = int.Parse(XALLWIDTHText.Text);
					FPGASender.ALL_HEIGHT = int.Parse(XALLHEIGHTText.Text);
					FPGASender.PANEL_RESOLUTION = int.Parse(XPANELRESOLUTIONText.Text);
					FPGASender.PANEL_BUFFER_SIZE = int.Parse(XPANELBUFFERSIZEText.Text);
					FPGASender.ALL_COUNT = int.Parse(XALLCOUNTText.Text);
					FPGASender.ALL_RESULUTION = int.Parse(XALLRESOLUTIONText.Text);
					FPGASender.ALL_BUFFER_SIZE = int.Parse(XALLBUFFERSIZEText.Text);
				}
				else
				{
					//自動計算
					FPGASender.UpdateSettingValue();
				}

				NotifyMessage(this, "値を更新しました。");
			}
			catch (Exception e)
			{
				NotifyMessage(e.Source, "値の更新に失敗しました。\n{0}", e.Message);
			}
		}
		private void LoadValue()
		{
			//定数値
			XPANELWIDTHText.Text = FPGASender.PANEL_WIDTH.ToString();
			XPANELHEIGHTText.Text = FPGASender.PANEL_HEIGHT.ToString();
			XPANELCHANNELSText.Text = FPGASender.PANEL_CHANNELS.ToString();
			XALLCOLUMNText.Text = FPGASender.ALL_COLUMN.ToString() ;
			XALLROWText.Text = FPGASender.ALL_ROW.ToString();
			XMASTERINTERVALText.Text = FPGASender.MASTER_INTERVAL.ToString();
			//設定値
			XALLWIDTHText.Text = FPGASender.ALL_WIDTH.ToString();
			XALLHEIGHTText.Text = FPGASender.ALL_HEIGHT.ToString();
			XPANELRESOLUTIONText.Text = FPGASender.PANEL_RESOLUTION.ToString();
			XPANELBUFFERSIZEText.Text = FPGASender.PANEL_BUFFER_SIZE.ToString();
			XALLCOUNTText.Text = FPGASender.ALL_COUNT.ToString();
			XALLRESOLUTIONText.Text = FPGASender.ALL_RESULUTION.ToString();
			XALLBUFFERSIZEText.Text = FPGASender.ALL_BUFFER_SIZE.ToString();
		}

		private void XUpdateValueButton_Click(object sender, RoutedEventArgs e)
		{
			//値を更新
			SetNewValue();
			//値の設定
			LoadValue();
		}

		/////////////////////////////////////////////////////////////////////////////////
		//通知以外は実装しない
		public bool Initialize()
		{
			throw new NotImplementedException();
		}

		public void Stop()
		{
			throw new NotImplementedException();
		}

		public bool Finalize()
		{
			throw new NotImplementedException();
		}

		public FPGASender.SendFromIplImageDelegate SendFromIplImage
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
				throw new NotImplementedException();
			}
		}
		//なんとなく
		public override string ToString()
		{
			return "fclp_c.SettingValueWindow";
		}
		private void XUnlockCalculationSetting_Checked(object sender, RoutedEventArgs e)
		{
			LoadValue();
		}


	}
}
