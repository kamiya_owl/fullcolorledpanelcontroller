﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DirectShowLib;
using System.Runtime.InteropServices;
using System.Windows.Threading;
using OpenCvSharp;
using System.Windows.Interop;
using DirectShowLib.Dvd;

namespace fclp_c
{
	/// <summary>
	/// DirectShowWindow.xaml の相互作用ロジック
	/// </summary>
	public partial class DirectShowWindow : Window, IFPGASendable, ISampleGrabberCB
	{
		//DirectShow
		//file
		IGraphBuilder _graphBuilder = null;//dvd
		ISampleGrabber _sampleGrabber = null;
		IBaseFilter _sampleGrabberFilter = null;

		IMediaControl _mediaControl = null;
		IMediaEvent _mediaEvent = null;//dvd
		IVideoWindow _videoWindow = null;//dvd
		IBasicAudio _basicAudio = null;//dvd
		IMediaSeeking _mediaSeeking = null;
		//DVD
		IDvdGraphBuilder _dvdGraphBuilder = null;//SendInfo
		VideoMixingRenderer9 _vmr9 = null;
		IBaseFilter _vmr9Filter = null;
		IVMRFilterConfig9 _vmrFilterConfig9 = null;
		IVMRWindowlessControl9 _vmrWindowlessControl9 = null;
		AMDvdRenderStatus _amDvdRenderStatus;//null非許容
		IDvdInfo2 _dvdInfo = null;
		IDvdControl2 _dvdControl = null;
		IMediaEventEx _mediaEventEx = null;
		IVideoFrameStep _videoFrameStep = null;//TODO:Fileにも実装
		//value
		int _videoWidth = 0;
		int _videoHeight = 0;
		int _imageStride = 0;
		int _imageSize = 0;
		//graph control
		int _hResult = 0;//例外
		int _volumeMin = -10000;//音量の最小値
		long _currentPosition = 0;//再生時間
		long _stopPosition = 0;//終了時間
		bool _seekBarUpdateChange = false;//排他処理的なもの
		bool _canFrameStep = false;//フレームステップ可能か
		//thread
		int _interval = FPGASender.MASTER_INTERVAL;//ms
		private DispatcherTimer _sampleObserver = null;
		//enum
		enum GraphState { NotInitialize,Stopped, Paused, Running, Exiting }
		volatile private GraphState _graphState = GraphState.NotInitialize;//マルチスレッドアクセス、最適化例外
		//delegate
		public FPGASender.SendFromIplImageDelegate SendFromIplImage { get; set; }
		public FPGASender.NotifyMessageDelegate NotifyMessage { get; set; }


		public DirectShowWindow()
		{
			InitializeComponent();
		}

		//////////////////////////////////////////////////////////////////////////////////////////////
		//DirectShow ErrorNotify(単一で例外処理を行いたい時に)
		private bool ThrowExceptionForHRWithNotifyMessage()
		{
			try
			{
				DsError.ThrowExceptionForHR(_hResult);
				return true;
			}
			catch(Exception e)
			{
				NotifyMessage(e.Source, "{0}", e.Message);
				return false;
			}
		}

		//////////////////////////////////////////////////////////////////////////////////////////////
		//Interface
		bool IFPGASendable.Initialize()
		{
			//既に存在する場合は一度開放する
			if (_graphState != GraphState.NotInitialize) FinalizeFilterGraph();
			//UI
			//XHiddenIsPlayCheck.IsChecked = true;
			return true;
		}
		void IFPGASendable.Stop()
		{
			this.Stop();
		}

		bool IFPGASendable.Finalize()
		{
			FinalizeFilterGraph();
			return true;
		}

		//////////////////////////////////////////////////////////////////////////////////////////////
		//DirectShow
		private bool InitializeFilterGraph(string filepath)
		{
			//既に存在する場合は一度開放する
			if (_graphState != GraphState.NotInitialize) FinalizeFilterGraph();
			_hResult = 0;
			//graph builder
			_graphBuilder = (IGraphBuilder)new FilterGraph();

			//sample grabber(have field)を生成
			_sampleGrabber = (ISampleGrabber)new SampleGrabber();
			_sampleGrabberFilter = (IBaseFilter)_sampleGrabber;
			//AMMediaTypeの設定
			AMMediaType amMediaType = new AMMediaType();
			amMediaType.majorType = MediaType.Video;
			amMediaType.subType = MediaSubType.RGB24;
			amMediaType.formatType = FormatType.VideoInfo;

			_hResult = _sampleGrabber.SetMediaType(amMediaType);
			DsError.ThrowExceptionForHR(_hResult);
			//callback
			//_sampleGrabber.SetCallback(this, 1);//1::BufferCBを呼び出す
			_hResult = _sampleGrabber.SetBufferSamples(true);//キャプチャを有効
			DsError.ThrowExceptionForHR(_hResult);
			//filter graphに追加
			_hResult = _graphBuilder.AddFilter(_sampleGrabberFilter, "Sample Grabber");
			DsError.ThrowExceptionForHR(_hResult);

			//VMR9を生成
			VideoMixingRenderer9 _vmr9 = new VideoMixingRenderer9();
			IBaseFilter _vmr9Filter = (IBaseFilter)_vmr9;
			//VMR9のInterface
			_vmrFilterConfig9 = (IVMRFilterConfig9)_vmr9;
			_vmrFilterConfig9.SetRenderingMode(VMR9Mode.Windowless);//windows less mode
			//control
			_vmrWindowlessControl9 = (IVMRWindowlessControl9)_vmr9;

			//filtergraphに追加
			_hResult = _graphBuilder.AddFilter(_vmr9Filter, "VMR9");
			DsError.ThrowExceptionForHR(_hResult);

			//Render以降の接続
			var pin_sampleGrabber_in = DsFindPin.ByDirection(_sampleGrabberFilter, PinDirection.Input, 0);
			var pin_sampleGrabber_out = DsFindPin.ByDirection(_sampleGrabberFilter, PinDirection.Output, 0);
			var pin_vmr9_in = DsFindPin.ByDirection(_vmr9Filter, PinDirection.Input, 0);
			_graphBuilder.Render(pin_sampleGrabber_in);
			_graphBuilder.Connect(pin_sampleGrabber_out, pin_vmr9_in);//[Inteligent connect] --(Render)--> SampleGrabber -> VMR9
			//接続、構築
			_hResult = _graphBuilder.RenderFile(filepath, filepath);
			DsError.ThrowExceptionForHR(_hResult);


			//SampleGrabberの情報を保存
			AMMediaType grabInfo = new AMMediaType();
			_hResult = _sampleGrabber.GetConnectedMediaType(grabInfo);
			DsError.ThrowExceptionForHR(_hResult);
			VideoInfoHeader videoInfoHeader = new VideoInfoHeader();
			Marshal.PtrToStructure(grabInfo.formatPtr, videoInfoHeader);
			//情報をフィールドにコピー
			_videoWidth = videoInfoHeader.BmiHeader.Width;
			_videoHeight = videoInfoHeader.BmiHeader.Height;
			_imageStride = videoInfoHeader.BmiHeader.ImageSize / _videoHeight;
			_imageSize = videoInfoHeader.BmiHeader.ImageSize;
			//開放
			DsUtils.FreeAMMediaType(grabInfo);

			//filter graph manager(have field)

			//ウインドウの設定
			_videoWindow = (IVideoWindow)_graphBuilder;
			//_videoWindow.put_Owner(((HwndSource)HwndSource.FromVisual(XpreviewImage)).Handle);//プレビューのハンドルを取得
			_videoWindow.put_Owner(new WindowInteropHelper(this).Handle);//ウインドウのハンドルを取得
			_videoWindow.put_WindowStyle(DirectShowLib.WindowStyle.Child | DirectShowLib.WindowStyle.ClipChildren);
			_videoWindow.SetWindowPosition(0, 0, (int)this.ActualWidth, (int)(this.ActualHeight - (XMediaControlGrid.ActualHeight + 30)));
			//特に設定しない場合別ウインドウに召喚する
			//...

			//再生・停止とか
			_mediaControl = (IMediaControl)_graphBuilder;
			//再生終了通知とか
			_mediaEvent = (IMediaEvent)_graphBuilder;
			//音量
			_basicAudio = (IBasicAudio)_graphBuilder;
			int volume = 0;
			_hResult = _basicAudio.get_Volume(out volume);
			DsError.ThrowExceptionForHR(_hResult);
			if (volume < _volumeMin) volume = _volumeMin;
			XVolumeSlider.Minimum = _volumeMin;
			XVolumeSlider.Value = volume;//スライダーに値を設定
			//シーク
			_mediaSeeking = (IMediaSeeking)_graphBuilder;
			_currentPosition = 0;//再生時間
			_stopPosition = 0;
			UpdateSeekBarValue();//スライダーの更新
			//他の値を変更
			XSeekBar.SmallChange = XSeekBar.Maximum / 40;
			XSeekBar.LargeChange = XSeekBar.Maximum / 20;
			XSeekBar.TickFrequency = XSeekBar.LargeChange;

			//スレッドの設定
			if (_sampleObserver != null)
			{
				if (_sampleObserver.IsEnabled) _sampleObserver.Stop();
				_sampleObserver = null;
			}
			_sampleObserver = new DispatcherTimer();
			_sampleObserver.Interval = new TimeSpan(0, 0, 0, 0, _interval);
			_sampleObserver.Tick += new EventHandler(_sampleObserver_Tick);
			_sampleObserver.Start();

			_graphState = GraphState.Stopped;
			XHiddenIsPlayCheck.IsChecked = true;//再生ボタンに戻しておく
			//開放
			GC.Collect();
			GC.WaitForPendingFinalizers();

			return true;
		}
		private bool InitializeFilterGraphForDVD()
		{
			//既に存在する場合は一度開放する
			if (_graphState != GraphState.NotInitialize) FinalizeFilterGraph();
			_hResult = 0;
			//graph builder
			_dvdGraphBuilder = (IDvdGraphBuilder)new DvdGraphBuilder();

			//Filter Graph (not DVDGraphBuilder)
			_hResult = _dvdGraphBuilder.GetFiltergraph(out _graphBuilder);

			//VMR9を生成
			VideoMixingRenderer9 _vmr9 = new VideoMixingRenderer9();
			IBaseFilter _vmr9Filter = (IBaseFilter)_vmr9;
			//VMR9のInterface
			_vmrFilterConfig9 = (IVMRFilterConfig9)_vmr9;
			_vmrFilterConfig9.SetRenderingMode(VMR9Mode.Windowless);//windows less mode
			//control
			_vmrWindowlessControl9 = (IVMRWindowlessControl9)_vmr9;


			//filtergraphに追加
			_hResult = _graphBuilder.AddFilter(_vmr9Filter, "VMR9");
			DsError.ThrowExceptionForHR(_hResult);

			//出力の構成をする
			var pin_vmr9_in = DsFindPin.ByDirection(_vmr9Filter, PinDirection.Input, 0);
			//接続
			_graphBuilder.Render(pin_vmr9_in);

			//自動構築
			_amDvdRenderStatus = new AMDvdRenderStatus();
			//Inteligent connect
			_hResult = _dvdGraphBuilder.RenderDvdVideoVolume(null, AMDvdGraphFlags.None, out _amDvdRenderStatus);
			DsError.ThrowExceptionForHR(_hResult);

			//DvdのInterface
			object comobj = null;//キャストで取得できない代わりにGetDvdInterfaceを使う

			//DvdInfo
			_hResult = _dvdGraphBuilder.GetDvdInterface(typeof(IDvdInfo2).GUID, out comobj);
			DsError.ThrowExceptionForHR(_hResult);
			_dvdInfo = (IDvdInfo2)comobj;
			comobj = null;

			//DvdControl
			_hResult = _dvdGraphBuilder.GetDvdInterface(typeof(IDvdControl2).GUID, out comobj);
			DsError.ThrowExceptionForHR(_hResult);
			_dvdControl = (IDvdControl2)comobj;
			comobj = null;

			//VideoWindow
			/*
			_dvdGraphBuilder.GetDvdInterface(typeof(IVideoWindow).GUID, out comobj);
			DsError.ThrowExceptionForHR(_hResult);
			_videoWindow = (IVideoWindow)comobj;
			comobj = null;
			//ウインドウの設定
			_videoWindow.put_Owner(new WindowInteropHelper(this).Handle);//ウインドウのハンドルを取得
			_videoWindow.put_WindowStyle(DirectShowLib.WindowStyle.Child | DirectShowLib.WindowStyle.ClipChildren);
			_videoWindow.SetWindowPosition(0, 0, (int)this.ActualWidth, (int)(this.ActualHeight - (XMediaControlGrid.ActualHeight + 30)));
			*/
			//MediaControl
			_mediaControl = (IMediaControl)_graphBuilder;

			//Media Event Ex
			_mediaEventEx = (IMediaEventEx)_graphBuilder;

			//VideoFrameStep(コマ送り)
			_canFrameStep = false;
			IVideoFrameStep _videoFrameStep = (IVideoFrameStep)_graphBuilder;
			_hResult = _videoFrameStep.CanStep(0, null);//コマ送り可能か判別
			if (_hResult == 0) _canFrameStep = true;

			//音量
			_basicAudio = (IBasicAudio)_graphBuilder;
			int volume = 0;
			_hResult = _basicAudio.get_Volume(out volume);
			DsError.ThrowExceptionForHR(_hResult);
			if (volume < _volumeMin) volume = _volumeMin;
			XVolumeSlider.Minimum = _volumeMin;
			XVolumeSlider.Value = volume;//スライダーに値を設定

			//シークは実装されていない
			//値の更新
			this.XSeekBar.Maximum = 0;
			this.XSeekBar.Value = 0;
			//テキストの更新
			XPlayingTimeText.Content = string.Format("00:00:00 / 00:00:00");

			/*
		   //スレッドの設定
		   if (_sampleObserver != null)
		   {
			   if (_sampleObserver.IsEnabled) _sampleObserver.Stop();
			   _sampleObserver = null;
		   }
		   _sampleObserver = new DispatcherTimer();
		   _sampleObserver.Interval = new TimeSpan(0, 0, 0, 0, interval);
		   _sampleObserver.Tick += new EventHandler(_sampleObserver_Tick);
		   _sampleObserver.Start();
			*/

			_graphState = GraphState.Stopped;
			XHiddenIsPlayCheck.IsChecked = true;//再生ボタンに戻しておく

			//開放
			GC.Collect();
			GC.WaitForPendingFinalizers();

			NotifyMessage(this, "DVDの読み込みに成功\nDvdFilterGraphManager\nVMR9 Windowless");
			return true;
		}
		//TODO:SampleGrabberを正しく動作させる
		private bool InitializeFilterGraphForDVDKai()
		{
			//既に存在する場合は一度開放する
			if (_graphState != GraphState.NotInitialize) FinalizeFilterGraph();
			_hResult = 0;
			//graph builder
			_dvdGraphBuilder = (IDvdGraphBuilder)new DvdGraphBuilder();

			//Filter Graph (not DVDGraphBuilder)
			_hResult = _dvdGraphBuilder.GetFiltergraph(out _graphBuilder);

			//sample grabber(have field)を生成
			_sampleGrabber = (ISampleGrabber)new SampleGrabber();
			_sampleGrabberFilter = (IBaseFilter)_sampleGrabber;
			//AMMediaTypeの設定
			AMMediaType amMediaType = new AMMediaType();
			amMediaType.majorType = MediaType.Video;
			amMediaType.subType = MediaSubType.RGB24;
			amMediaType.formatType = FormatType.VideoInfo;

			_hResult = _sampleGrabber.SetMediaType(amMediaType);
			DsError.ThrowExceptionForHR(_hResult);
			//callback
			//_sampleGrabber.SetCallback(this, 1);//1::BufferCBを呼び出す
			_hResult = _sampleGrabber.SetBufferSamples(true);//キャプチャを有効
			DsError.ThrowExceptionForHR(_hResult);
			//filter graphに追加
			_hResult = _graphBuilder.AddFilter(_sampleGrabberFilter, "Sample Grabber");
			DsError.ThrowExceptionForHR(_hResult);

			//VMR9を生成
			VideoMixingRenderer9 _vmr9 = new VideoMixingRenderer9();
			IBaseFilter _vmr9Filter = (IBaseFilter)_vmr9;
			//VMR9のInterface
			_vmrFilterConfig9 = (IVMRFilterConfig9)_vmr9;
			_vmrFilterConfig9.SetRenderingMode(VMR9Mode.Windowless);//windows less mode
			//control
			_vmrWindowlessControl9 = (IVMRWindowlessControl9)_vmr9;


			//filtergraphに追加
			_hResult = _graphBuilder.AddFilter(_vmr9Filter, "VMR9");
			DsError.ThrowExceptionForHR(_hResult);

			//出力の構成をする
			var pin_sampleGrabber_in = DsFindPin.ByDirection(_sampleGrabberFilter, PinDirection.Input, 0);
			var pin_sampleGrabber_out = DsFindPin.ByDirection(_sampleGrabberFilter, PinDirection.Output, 0);
			var pin_vmr9_in = DsFindPin.ByDirection(_vmr9Filter, PinDirection.Input, 0);
			//接続
			_graphBuilder.Render(pin_sampleGrabber_in);
			_graphBuilder.Connect(pin_sampleGrabber_out, pin_vmr9_in);

			//自動構築
			_amDvdRenderStatus = new AMDvdRenderStatus();
			//Inteligent connect
			_hResult = _dvdGraphBuilder.RenderDvdVideoVolume(null, AMDvdGraphFlags.None, out _amDvdRenderStatus);
			DsError.ThrowExceptionForHR(_hResult);

			//DvdのInterface
			object comobj = null;//キャストで取得できない代わりにGetDvdInterfaceを使う

			//DvdInfo
			_hResult = _dvdGraphBuilder.GetDvdInterface(typeof(IDvdInfo2).GUID, out comobj);
			DsError.ThrowExceptionForHR(_hResult);
			_dvdInfo = (IDvdInfo2)comobj;
			comobj = null;

			//DvdControl
			_hResult = _dvdGraphBuilder.GetDvdInterface(typeof(IDvdControl2).GUID, out comobj);
			DsError.ThrowExceptionForHR(_hResult);
			_dvdControl = (IDvdControl2)comobj;
			comobj = null;

			//VideoWindow
			/*
			_dvdGraphBuilder.GetDvdInterface(typeof(IVideoWindow).GUID, out comobj);
			DsError.ThrowExceptionForHR(_hResult);
			_videoWindow = (IVideoWindow)comobj;
			comobj = null;
			//ウインドウの設定
			_videoWindow.put_Owner(new WindowInteropHelper(this).Handle);//ウインドウのハンドルを取得
			_videoWindow.put_WindowStyle(DirectShowLib.WindowStyle.Child | DirectShowLib.WindowStyle.ClipChildren);
			_videoWindow.SetWindowPosition(0, 0, (int)this.ActualWidth, (int)(this.ActualHeight - (XMediaControlGrid.ActualHeight + 30)));
			*/
			//MediaControl
			_mediaControl = (IMediaControl)_graphBuilder;

			//Media Event Ex
			_mediaEventEx = (IMediaEventEx)_graphBuilder;

			//VideoFrameStep(コマ送り)
			_canFrameStep = false;
			IVideoFrameStep _videoFrameStep = (IVideoFrameStep)_graphBuilder;
			_hResult = _videoFrameStep.CanStep(0, null);//コマ送り可能か判別
			if (_hResult == 0) _canFrameStep = true;

			//音量
			_basicAudio = (IBasicAudio)_graphBuilder;
			int volume = 0;
			_hResult = _basicAudio.get_Volume(out volume);
			DsError.ThrowExceptionForHR(_hResult);
			if (volume < _volumeMin) volume = _volumeMin;
			XVolumeSlider.Minimum = _volumeMin;
			XVolumeSlider.Value = volume;//スライダーに値を設定

			//シークは実装されていない
			//値の更新
			this.XSeekBar.Maximum = 0;
			this.XSeekBar.Value = 0;
			//テキストの更新
			XPlayingTimeText.Content = string.Format("00:00:00 / 00:00:00");

			/*
		   //スレッドの設定
		   if (_sampleObserver != null)
		   {
			   if (_sampleObserver.IsEnabled) _sampleObserver.Stop();
			   _sampleObserver = null;
		   }
		   _sampleObserver = new DispatcherTimer();
		   _sampleObserver.Interval = new TimeSpan(0, 0, 0, 0, interval);
		   _sampleObserver.Tick += new EventHandler(_sampleObserver_Tick);
		   _sampleObserver.Start();
			*/

			_graphState = GraphState.Stopped;
			XHiddenIsPlayCheck.IsChecked = true;//再生ボタンに戻しておく

			//開放
			GC.Collect();
			GC.WaitForPendingFinalizers();

			NotifyMessage(this, "DVDの読み込みに成功\nDvdFilterGraphManager\nVMR9 Windowless");
			return true;
		}
		private bool FinalizeFilterGraph()
		{
			if (_sampleObserver != null)
			{
				_sampleObserver.Stop();
				_sampleObserver = null;
			}
			if (_mediaControl != null)
			{
				_mediaControl.Stop();
				_mediaControl = null;
			}
			if (_mediaEvent != null)
			{
				_mediaEvent = null;
			}
			if (_videoWindow != null)
			{
				_videoWindow = null;
			}
			if (_basicAudio != null)
			{
				_basicAudio = null;
			}
			if (_mediaSeeking != null)
			{
				_mediaSeeking = null;
			}
			if (_sampleGrabber != null)
			{
				Marshal.ReleaseComObject(_sampleGrabber);
				_sampleGrabber = null;
			}
			if (_graphBuilder != null)
			{
				Marshal.ReleaseComObject(_graphBuilder);
				_graphBuilder = null;
			}
			//
			_graphState = GraphState.NotInitialize;
			//ガベージコレクション
			GC.Collect();
			GC.WaitForPendingFinalizers();//待っててあげる

			return true;
		}
		int ISampleGrabberCB.BufferCB(double SampleTime, IntPtr pBuffer, int BufferLen)
		{
			throw new NotImplementedException();
		}

		int ISampleGrabberCB.SampleCB(double SampleTime, IMediaSample pSample)
		{
			throw new NotImplementedException();
		}
		private bool Start()//with Resume
		{
			int hResult = 0;
			switch(_graphState)
			{
				case GraphState.Stopped:
				case GraphState.Paused:
					hResult = _mediaControl.Run();
					DsError.ThrowExceptionForHR(hResult);
					_graphState = GraphState.Running;
					NotifyMessage(this, "動画の再生を開始しました。");
					return true;
				default:
					return false;					
			}
		}
		private bool Pause()
		{
			int hResult = 0;
			switch(_graphState)
			{
				case GraphState.Running:
					hResult = _mediaControl.Pause();
					DsError.ThrowExceptionForHR(hResult);
					_graphState = GraphState.Paused;
					NotifyMessage(this, "動画を一時停止しました。");
					return true;
				default:
					return false;					
			}
		}
		private bool Stop()
		{
			int hResult = 0;
			switch(_graphState)
			{
				case GraphState.Running:
					hResult = _mediaControl.Stop();
					DsError.ThrowExceptionForHR(hResult);
					_graphState = GraphState.Stopped;
					NotifyMessage(this, "動画を停止しました。");
					return true;
				default:
					return false;
			}
		}
		//////////////////////////////////////////////////////////////////////////////////////////////
		//SampleGrabber Thread
		void _sampleObserver_Tick(object sender, EventArgs e)
		{
			//更新
			UpdateSeekBarValue();

			//転送
			IntPtr buffer_pointer = IntPtr.Zero;

			//unsafeメモリを確保して、bufferを読めるようにする
			buffer_pointer = Marshal.AllocCoTaskMem(_imageSize);
			//取得
			_sampleGrabber.GetCurrentBuffer(ref _imageSize, buffer_pointer);
			if (SendFromIplImage == null) return;
			//IplImageに変換
			IplImage image = new IplImage(_videoWidth, _videoHeight, BitDepth.U8, 3);
			image.ImageData = buffer_pointer;//参照を変更(既にSampleGrabberによって確保したものを読ませる)

			//Delegateに送信
			SendFromIplImage(image,true);//sampleGrabberで取得できる画像は既に反転している
			//window.ShowImage(image);

			//Release
			//buffer = null;
			image.Dispose();
			image = null;
			Marshal.FreeCoTaskMem(buffer_pointer);//AllocCoTaskMemと対にする
			buffer_pointer = IntPtr.Zero;
		}

		//////////////////////////////////////////////////////////////////////////////////////////////
		//Event
		private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
		{
			//DirectShow Overlayする位置を変更
			if (_videoWindow != null)
			{
				_hResult = _videoWindow.SetWindowPosition(0, 0, (int)this.ActualWidth, (int)(this.ActualHeight - (XMediaControlGrid.ActualHeight + 30)));//30 = XSeekBar.Height
				ThrowExceptionForHRWithNotifyMessage();
			}
		}
		private void FileOpen(string filename)
		{
			//Load
			try
			{
				InitializeFilterGraph(filename);
				this.Title = string.Format("DirectShow MoviePlay File = \'{0}\'", filename);
				NotifyMessage(this, "動画の読み込み完了が完了しました。\n{0}", filename);
			}
			catch (Exception ex)
			{
				NotifyMessage(ex.Source, "動画の読み込みに失敗。\n{0}", ex.Message);
			}
		}
		private void XOpenFileButton_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{

			var dialog = new Microsoft.Win32.OpenFileDialog();
			dialog.FileName = "";
			dialog.DefaultExt = "*.*";
			if ((bool)dialog.ShowDialog())
			{
				FileOpen(dialog.FileName);
			}
		}
		private void Window_DragOver(object sender, DragEventArgs e)
		{
			e.Handled = true;
		}
		private void Window_Drop(object sender, DragEventArgs e)
		{
			//ドロップされた画像ファイルを出力
			string[] filename = (string[])e.Data.GetData(DataFormats.FileDrop);
			if (filename.Length > 1) NotifyMessage(this, "複数ファイルのドロップは行うことができません。{0}を読み込みます。", filename[0]);
			FileOpen(filename[0]);
		}


		private void XEnableRepeatButton_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			//反転（アニメーションバインドが適用されている）
			XHiddenIsLoop.IsChecked = !(bool)XHiddenIsLoop.IsChecked;
		}

		private void XPlayPauseButton_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			if ((bool)XHiddenIsPlayCheck.IsChecked)
			{
				//Play
				if (Start())
				{
					//再生開始
					XHiddenIsPlayCheck.IsChecked = false;
				}
			}
			else
			{
				//Pause
				if (Pause())
				{
					//再生停止
					XHiddenIsPlayCheck.IsChecked = true;
				}
			}

		}

		private void XVolumeSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
		{
			if (_basicAudio != null)
			{
				//Volume
				_hResult = _basicAudio.put_Volume((int)e.NewValue);
				ThrowExceptionForHRWithNotifyMessage();
			}
		}

		private void UpdateSeekBarValue()
		{
			if (_mediaSeeking != null)
			{
				//Seekbarの値を更新
				_hResult = _mediaSeeking.GetPositions(out _currentPosition, out _stopPosition);
				if (!ThrowExceptionForHRWithNotifyMessage()) return;
				//排他ロック
				_seekBarUpdateChange = true;
				//値の更新
				this.XSeekBar.Maximum = _stopPosition;
				this.XSeekBar.Value = _currentPosition;
				//テキストの更新
				TimeSpan current = new TimeSpan(0, 0, 0, (int)(_currentPosition / 10000000));
				TimeSpan stop = new TimeSpan(0, 0, 0, (int)(_stopPosition / 10000000));
				XPlayingTimeText.Content = string.Format("{0:d2}:{1:d2}:{2:d2} / {3:d2}:{4:d2}:{5:d2}",current.Hours,current.Minutes,current.Seconds,stop.Hours,stop.Minutes,stop.Seconds );
				//ロック解除
				_seekBarUpdateChange = false;
				//再生終了時ループ処理(ロック外での処理)
				if ((XHiddenIsLoop.IsChecked == true) && (XSeekBar.Value == XSeekBar.Maximum))
				{
					XSeekBar.Value = 0;
					NotifyMessage(this, "動画をループ再生しました。");
				}
			}
		}
		private void XSeekBar_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
		{
			//UpdateSeekBarValue -> ValueChangedの場合を弾く
			if (_seekBarUpdateChange) return;

			if (_mediaSeeking != null)
			{
				//値を逆向きに取得
				_currentPosition = (long)XSeekBar.Value;
				_stopPosition = (long)XSeekBar.Maximum;
				//シークバーの値をIMediaSeekingに送る (更新,絶対値指定|近いキーフレームをシーク,停止位置,停止位置の変更はない)
				_hResult = _mediaSeeking.SetPositions(_currentPosition, AMSeekingSeekingFlags.AbsolutePositioning | AMSeekingSeekingFlags.SeekToKeyFrame, _stopPosition, AMSeekingSeekingFlags.NoPositioning);
				if (!ThrowExceptionForHRWithNotifyMessage()) return;
				//通知（連射されるから非推奨）
				//NotifyMessage(this, "再生位置を変更しました。\ncurrent = {0}\nstop = {1}", _currentPosition, _stopPosition);
			}
			else
			{
				//0にしておく
				XSeekBar.Value = 0;
			}
		}

		private void XBackButton_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			//_seekBarUpdateChangeをfalseのまま値を変更
			XSeekBar.Value = 0;
			if (_mediaSeeking != null) NotifyMessage(this, "再生位置を先頭まで移動しました。");
		}

		private void XNextButton_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			//_seekBarUpdateChangeをfalseのまま値を変更
			XSeekBar.Value = XSeekBar.Maximum;
			if (_mediaSeeking != null) NotifyMessage(this, "再生位置を最後まで移動しました。");
		}

		private void XPlayingDVDGrid_MouseDown(object sender, MouseButtonEventArgs e)
		{
			InitializeFilterGraphForDVD();
		}



		/// ///////////////////////////TESTTESTTEST//////////////////////////////////
		//CvWindow window = new CvWindow("capture", WindowMode.FreeRatio);
		private void button1_Click(object sender, RoutedEventArgs e)
		{
			/*
			IntPtr buffer_pointer = IntPtr.Zero;
			int width,height,a_width,a_height;
			//_vmrWindowlessControl9.GetNativeVideoSize(out width, out height, out a_width, out a_height);
			_hResult = _vmrWindowlessControl9.GetCurrentImage(out buffer_pointer);//TODO:エラー改善
			DsError.ThrowExceptionForHR(_hResult);

			IplImage capture = new IplImage(buffer_pointer);
			window.ShowImage(capture);
			 */
		}


	}
}
